USE [master]
GO
/****** Object:  Database [BDv_log]    Script Date: 8/19/2019 8:52:47 AM ******/
CREATE DATABASE [BDv_log]
GO

ALTER DATABASE [BDv_log] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BDv_log].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BDv_log] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BDv_log] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BDv_log] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BDv_log] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BDv_log] SET ARITHABORT OFF 
GO
ALTER DATABASE [BDv_log] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BDv_log] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BDv_log] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BDv_log] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BDv_log] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BDv_log] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BDv_log] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BDv_log] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BDv_log] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BDv_log] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BDv_log] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BDv_log] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BDv_log] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BDv_log] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BDv_log] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BDv_log] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BDv_log] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BDv_log] SET RECOVERY FULL 
GO
ALTER DATABASE [BDv_log] SET  MULTI_USER 
GO
ALTER DATABASE [BDv_log] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BDv_log] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BDv_log] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BDv_log] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BDv_log] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BDv_log', N'ON'
GO
ALTER DATABASE [BDv_log] SET QUERY_STORE = OFF
GO
USE [BDv_log]
GO
/****** Object:  UserDefinedFunction [dbo].[mantenimientoHardware]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Oscar Sanchez
-- Create date: 14-08-2019
-- Description:	Funcion que se ingresa el id del harware y retorna la fecha de mantenimiento de este 
-- =============================================
CREATE FUNCTION [dbo].[mantenimientoHardware] 
(
	@idTecnologia varchar (10)
)
RETURNS date
AS
BEGIN
	
	DECLARE @fecha date

	
	SELECT @fecha = mh.FechaMantenimiento
	From dbo.TbHardware H inner join dbo.TbMantenimientoHardware mh on h.IdMantenimientoHardware = mh.IdMantenimientoHardware 
	where @idTecnologia = h.IdTecnologia
	
	RETURN @fecha

END
GO
/****** Object:  Table [dbo].[TbActividad]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbActividad](
	[codigo] [varchar](10) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TbActividad] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbActividadesXproyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbActividadesXproyecto](
	[idActividadesXproyecto] [int] IDENTITY(1,1) NOT NULL,
	[codigoActividades] [varchar](10) NOT NULL,
	[codigoProyecto] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TbActividadesXproyecto] PRIMARY KEY CLUSTERED 
(
	[idActividadesXproyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbBitacora]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbBitacora](
	[idBitacora] [int] IDENTITY(1,1) NOT NULL,
	[codigoProyecto] [varchar](10) NOT NULL,
	[proyecto] [varchar](50) NOT NULL,
	[actividad] [varchar](50) NOT NULL,
	[fechaInicio] [date] NOT NULL,
	[fechaFin] [date] NOT NULL,
	[horaInicio] [time](7) NOT NULL,
	[horaFin] [time](7) NOT NULL,
 CONSTRAINT [PK_TbBitacora] PRIMARY KEY CLUSTERED 
(
	[idBitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbClienteFi]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbClienteFi](
	[cedula] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido1] [varchar](50) NOT NULL,
	[apellido2] [varchar](50) NOT NULL,
	[provincia] [varchar](50) NOT NULL,
	[canton] [varchar](50) NOT NULL,
	[distrito] [varchar](50) NOT NULL,
	[direccion] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[telefono] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TbClienteFi] PRIMARY KEY CLUSTERED 
(
	[cedula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbClienteJu]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbClienteJu](
	[cedulaJuridica] [int] NOT NULL,
	[nombreCliente] [varchar](50) NOT NULL,
	[provincia] [varchar](50) NOT NULL,
	[canton] [varchar](50) NOT NULL,
	[distrito] [varchar](50) NOT NULL,
	[direccion] [varchar](50) NOT NULL,
	[cedula] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido1] [varchar](50) NOT NULL,
	[apellido2] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[telefono] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TbClienteJuridico] PRIMARY KEY CLUSTERED 
(
	[cedulaJuridica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbProyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbProyecto](
	[codigoProyecto] [varchar](10) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[fechaInicio] [date] NOT NULL,
	[fechaFin] [date] NOT NULL,
	[tipoProyecto] [varchar](50) NOT NULL,
	[cliente] [varchar](100) NULL,
 CONSTRAINT [PK_TbProyecto] PRIMARY KEY CLUSTERED 
(
	[codigoProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbReporte]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbReporte](
	[idReporte] [int] IDENTITY(1,1) NOT NULL,
	[proyecto] [varchar](50) NOT NULL,
	[actividad] [varchar](50) NOT NULL,
	[tiempoInvertido] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TbReportes] PRIMARY KEY CLUSTERED 
(
	[idReporte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbTecnologia]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbTecnologia](
	[codigo] [varchar](10) NOT NULL,
	[tecnologia] [varchar](50) NOT NULL,
 CONSTRAINT [PkTbTecnologiasCodigo] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbTecnologiasXproyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbTecnologiasXproyecto](
	[idTecnologiasXproyecto] [int] IDENTITY(1,1) NOT NULL,
	[codigoTecnologias] [varchar](10) NOT NULL,
	[codigoProyecto] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TbTecnologiasXproyecto_1] PRIMARY KEY CLUSTERED 
(
	[idTecnologiasXproyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbUsuario]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbUsuario](
	[cedula] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido1] [varchar](50) NOT NULL,
	[apellido2] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[genero] [varchar](50) NOT NULL,
	[avatar] [varchar](300) NULL,
	[contrasena] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TbUsuario] PRIMARY KEY CLUSTERED 
(
	[cedula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[TbActividad] ([codigo], [nombre], [descripcion]) VALUES (N'ACT1', N'Programar Lógica', N'back-end')
INSERT [dbo].[TbActividad] ([codigo], [nombre], [descripcion]) VALUES (N'ACT2', N'Programar UI', N'front-end')
SET IDENTITY_INSERT [dbo].[TbActividadesXproyecto] ON 

INSERT [dbo].[TbActividadesXproyecto] ([idActividadesXproyecto], [codigoActividades], [codigoProyecto]) VALUES (13, N'ACT1', N'PYT1')
INSERT [dbo].[TbActividadesXproyecto] ([idActividadesXproyecto], [codigoActividades], [codigoProyecto]) VALUES (18, N'ACT2', N'PYT1')
SET IDENTITY_INSERT [dbo].[TbActividadesXproyecto] OFF
SET IDENTITY_INSERT [dbo].[TbBitacora] ON 

INSERT [dbo].[TbBitacora] ([idBitacora], [codigoProyecto], [proyecto], [actividad], [fechaInicio], [fechaFin], [horaInicio], [horaFin]) VALUES (14, N'PYT1', N'V-Log', N'Programar Lógica', CAST(N'2019-06-03' AS Date), CAST(N'2019-08-19' AS Date), CAST(N'13:00:00' AS Time), CAST(N'19:00:00' AS Time))
SET IDENTITY_INSERT [dbo].[TbBitacora] OFF
INSERT [dbo].[TbClienteFi] ([cedula], [nombre], [apellido1], [apellido2], [provincia], [canton], [distrito], [direccion], [email], [telefono]) VALUES (123, N'Mariel', N'Barboza', N'Salazar', N'Heredia', N'Santo Domingo', N'Santo Domingo', N'Barrio Falso', N'mariel@test.com', N'88880000')
INSERT [dbo].[TbClienteFi] ([cedula], [nombre], [apellido1], [apellido2], [provincia], [canton], [distrito], [direccion], [email], [telefono]) VALUES (456, N'Pablo', N'Porras', N'Vargas', N'San Jose', N'Montes de Oca', N'Sabanilla', N'Sabanilla centro', N'ppz@gmail.com', N'22333337')
INSERT [dbo].[TbClienteJu] ([cedulaJuridica], [nombreCliente], [provincia], [canton], [distrito], [direccion], [cedula], [nombre], [apellido1], [apellido2], [email], [telefono]) VALUES (766, N'Shout S.A', N'San Jose', N'Montes de Oca', N'Los Yoses', N'Los yoses sur.', 12387660, N'Daniel', N'Del Risco', N'Bonilla', N'daniel@test.com', N'87609400')
INSERT [dbo].[TbClienteJu] ([cedulaJuridica], [nombreCliente], [provincia], [canton], [distrito], [direccion], [cedula], [nombre], [apellido1], [apellido2], [email], [telefono]) VALUES (39484, N'MarteStudio', N'San Jose', N'Sabana Norte', N'Mata Redonda', N'150 NE del ICE', 178906543, N'Hans', N'Castro', N'Navarro', N'hans@test.com', N'22333330')
INSERT [dbo].[TbProyecto] ([codigoProyecto], [nombre], [descripcion], [fechaInicio], [fechaFin], [tipoProyecto], [cliente]) VALUES (N'PYT1', N'V-Log', N'Proyecto programado', CAST(N'2019-06-03' AS Date), CAST(N'2019-08-19' AS Date), N'Verónica Morera Carballo', N'Académico')
SET IDENTITY_INSERT [dbo].[TbReporte] ON 

INSERT [dbo].[TbReporte] ([idReporte], [proyecto], [actividad], [tiempoInvertido]) VALUES (11, N'V-Log', N'Programar Lógica', N'07:30:22')
SET IDENTITY_INSERT [dbo].[TbReporte] OFF
INSERT [dbo].[TbTecnologia] ([codigo], [tecnologia]) VALUES (N'TEC01', N'Java')
INSERT [dbo].[TbTecnologia] ([codigo], [tecnologia]) VALUES (N'TEC02', N'SQL')
INSERT [dbo].[TbTecnologia] ([codigo], [tecnologia]) VALUES (N'TEC03', N'Python')
INSERT [dbo].[TbTecnologia] ([codigo], [tecnologia]) VALUES (N'TEC04', N'Node.Js')
INSERT [dbo].[TbTecnologia] ([codigo], [tecnologia]) VALUES (N'TEC05', N'Ruby')
SET IDENTITY_INSERT [dbo].[TbTecnologiasXproyecto] ON 

INSERT [dbo].[TbTecnologiasXproyecto] ([idTecnologiasXproyecto], [codigoTecnologias], [codigoProyecto]) VALUES (38, N'TEC01', N'PYT1')
INSERT [dbo].[TbTecnologiasXproyecto] ([idTecnologiasXproyecto], [codigoTecnologias], [codigoProyecto]) VALUES (44, N'TEC02', N'PYT1')
SET IDENTITY_INSERT [dbo].[TbTecnologiasXproyecto] OFF
INSERT [dbo].[TbUsuario] ([cedula], [nombre], [apellido1], [apellido2], [email], [genero], [avatar], [contrasena]) VALUES (1234, N'Verónica', N'Morera', N'Carballo', N'vero@test.com', N'Femenino', N'src\images\perritoProgramador.jpg', N'123')
ALTER TABLE [dbo].[TbActividadesXproyecto]  WITH CHECK ADD  CONSTRAINT [FK_TbActividadesXproyecto_TbActividad] FOREIGN KEY([codigoActividades])
REFERENCES [dbo].[TbActividad] ([codigo])
GO
ALTER TABLE [dbo].[TbActividadesXproyecto] CHECK CONSTRAINT [FK_TbActividadesXproyecto_TbActividad]
GO
ALTER TABLE [dbo].[TbActividadesXproyecto]  WITH CHECK ADD  CONSTRAINT [FK_TbActividadesXproyecto_TbProyecto] FOREIGN KEY([codigoProyecto])
REFERENCES [dbo].[TbProyecto] ([codigoProyecto])
GO
ALTER TABLE [dbo].[TbActividadesXproyecto] CHECK CONSTRAINT [FK_TbActividadesXproyecto_TbProyecto]
GO
ALTER TABLE [dbo].[TbTecnologiasXproyecto]  WITH CHECK ADD  CONSTRAINT [FkTbTecnologiasXproyectoTbProyectoCodigoProyecto] FOREIGN KEY([codigoProyecto])
REFERENCES [dbo].[TbProyecto] ([codigoProyecto])
GO
ALTER TABLE [dbo].[TbTecnologiasXproyecto] CHECK CONSTRAINT [FkTbTecnologiasXproyectoTbProyectoCodigoProyecto]
GO
ALTER TABLE [dbo].[TbTecnologiasXproyecto]  WITH CHECK ADD  CONSTRAINT [FkTbTecnologiasXproyectoTbTecnologiasCodigoTecnologia] FOREIGN KEY([codigoTecnologias])
REFERENCES [dbo].[TbTecnologia] ([codigo])
GO
ALTER TABLE [dbo].[TbTecnologiasXproyecto] CHECK CONSTRAINT [FkTbTecnologiasXproyectoTbTecnologiasCodigoTecnologia]
GO
/****** Object:  StoredProcedure [dbo].[asociarActividadXproyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[asociarActividadXproyecto]
(
	@codigoActividad varchar(10),
	@codigoProyecto varchar(10)
)

AS
BEGIN


   INSERT INTO TbActividadesXproyecto
   (
    codigoActividades,
	codigoProyecto
   )
  VALUES
  (
  @codigoActividad,
  @codigoProyecto
  )

END
GO
/****** Object:  StoredProcedure [dbo].[asociarTecnologiaXproyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[asociarTecnologiaXproyecto]
(
	@codigoTecnologia varchar(10),
	@codigoProyecto varchar(10)
)

AS
BEGIN

   INSERT INTO TbTecnologiasXproyecto
   (
    codigoTecnologias,
	codigoProyecto
   )
  VALUES
  (
  @codigoTecnologia,
  @codigoProyecto
  )

END
GO
/****** Object:  StoredProcedure [dbo].[buscarActividadXcodigo]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[buscarActividadXcodigo]
(
	@codigo varchar(10)
)
AS
BEGIN

SET nocount on;

   SELECT CASE WHEN EXISTS (
      (SELECT *
      FROM TbActividad
      WHERE codigo = @codigo) 
   )
   THEN Cast(1 AS varchar) 
   ELSE Cast(0 AS varchar) 
   END AS 'resultado'

END
GO
/****** Object:  StoredProcedure [dbo].[buscarActividadXnombre]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[buscarActividadXnombre]
(
	@nombre varchar(50)
)
AS
BEGIN

SET nocount on;

   SELECT CASE WHEN EXISTS (
      (SELECT *
      FROM TbActividad
      WHERE nombre = @nombre) 
   )
   THEN Cast(1 AS varchar) 
   ELSE Cast(0 AS varchar) 
   END AS 'resultado'

END
GO
/****** Object:  StoredProcedure [dbo].[buscarClienteFisico]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[buscarClienteFisico]
(
	@cedula int)
AS
BEGIN

SET nocount on;

   SELECT CASE WHEN EXISTS (
      (SELECT *
	  FROM TbClienteFi
      WHERE cedula = @cedula) 
   )
   THEN Cast(1 AS varchar) 
   ELSE Cast(0 AS varchar) 
   END AS 'resultado'

END
GO
/****** Object:  StoredProcedure [dbo].[buscarClienteJuridico]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[buscarClienteJuridico]
(
	@cedulaJuridica int,
	@nombreCliente varchar(50)
)
AS
BEGIN

SET nocount on;

   SELECT CASE WHEN EXISTS (
      (SELECT *
	  FROM TbClienteJu
      WHERE cedulaJuridica = @cedulaJuridica OR nombreCliente = @nombreCliente) 
   )
   THEN Cast(1 AS varchar) 
   ELSE Cast(0 AS varchar) 
   END AS 'resultado'

END
GO
/****** Object:  StoredProcedure [dbo].[buscarProyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[buscarProyecto]
(
	@codigoProyecto varchar(10),
	@nombre varchar(50)
)
AS
BEGIN

SET nocount on;

   SELECT CASE WHEN EXISTS (
      (SELECT *
	  FROM TbProyecto
      WHERE codigoProyecto = @codigoProyecto OR nombre = @nombre) 
   )
   THEN Cast(1 AS varchar) 
   ELSE Cast(0 AS varchar) 
   END AS 'resultado'

END
GO
/****** Object:  StoredProcedure [dbo].[buscarTecnologia]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[buscarTecnologia]
(
	@codigo varchar(10),
	@tecnologia varchar(50)
)
AS
BEGIN

SET nocount on;

   SELECT CASE WHEN EXISTS (
      (SELECT *
	  FROM TbTecnologia
      WHERE codigo = @codigo OR tecnologia = @tecnologia) 
   )
   THEN Cast(1 AS varchar) 
   ELSE Cast(0 AS varchar) 
   END AS 'resultado'

END
GO
/****** Object:  StoredProcedure [dbo].[buscarUsuario]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[buscarUsuario]
(
	@email varchar(50)
)
AS
BEGIN

SET nocount on;

   SELECT CASE WHEN EXISTS (
      (SELECT *
	  FROM TbUsuario
      WHERE email = @email) 
   )
   THEN Cast(1 AS varchar) 
   ELSE Cast(0 AS varchar) 
   END AS 'resultado'

END
GO
/****** Object:  StoredProcedure [dbo].[editarClienteFisico]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editarClienteFisico]
(
	@cedula int, 
	@nombre varchar(50),
	@apellido1 varchar(50),
	@apellido2 varchar(50),
	@provincia varchar(50),
	@canton varchar(50),
	@distrito varchar(50),
	@direccion varchar(50),
	@email varchar(50),
	@telefono varchar(50)
)

AS
BEGIN

	UPDATE TbClienteFi SET 
							cedula = @cedula, 
							nombre = @nombre,
							apellido1 = @apellido1,
							apellido2 = @apellido2,
							provincia = @provincia,
							canton = @canton,
							distrito = @distrito,
							direccion = @direccion,
							email = @email,
							telefono = @telefono

					WHERE  cedula = @cedula 
	
	SELECT * FROM TbClienteJu WHERE cedula = @cedula  
		
END
GO
/****** Object:  StoredProcedure [dbo].[editarClienteJuridico]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editarClienteJuridico]
(
	@cedulaJuridica int,
	@nombreCliente varchar(50),
	@provincia varchar(50),
	@canton varchar(50),
	@distrito varchar(50),
	@direccion varchar(50),
	@cedula int, 
	@nombre varchar(50),
	@apellido1 varchar(50),
	@apellido2 varchar(50),
	@email varchar(50),
	@telefono varchar(50)
)

AS
BEGIN

	UPDATE TbClienteJu SET 
							nombreCliente = @nombreCliente,
							provincia = @provincia,
							canton = @canton,
							distrito = @distrito,
							direccion = @direccion,
							cedula = @cedula, 
							nombre = @nombre,
							apellido1 = @apellido1,
							apellido2 = @apellido2,
							email = @email,
							telefono = @telefono

					WHERE  cedulaJuridica = @cedulaJuridica  
	
	SELECT * FROM TbClienteJu WHERE cedulaJuridica = @cedulaJuridica  
		
END
GO
/****** Object:  StoredProcedure [dbo].[editarProyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editarProyecto]
(
	@codigoProyecto varchar(10),
	@nombre varchar(50),
	@descripcion varchar(100),
	@fechaInicio date,
	@fechaFin date,
	@tipoProyecto varchar(50) ,
	@cliente varchar(100)
	)
AS
BEGIN

	UPDATE TbProyecto SET 
						nombre = @nombre,
						descripcion = @descripcion,
						fechaInicio = @fechaInicio,
						fechaFin = @fechaFin,
						tipoProyecto = @tipoProyecto,
						cliente = @cliente

					WHERE  codigoProyecto = @codigoProyecto  
	
	SELECT * FROM TbProyecto WHERE  codigoProyecto = @codigoProyecto  
		
END
GO
/****** Object:  StoredProcedure [dbo].[editarTecnologia]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editarTecnologia]
(
	@codigo varchar(10),
	@tecnologia varchar(50)
)

AS
BEGIN

	UPDATE TbTecnologia SET 
					tecnologia= @tecnologia
					WHERE  codigo = @codigo  
	
	SELECT * FROM TbTecnologia WHERE  codigo = @codigo  
		
END
GO
/****** Object:  StoredProcedure [dbo].[editarUsuarioAvatar]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editarUsuarioAvatar]
(
	@cedula int,
	@avatar varchar(300)
)

AS
BEGIN

UPDATE TbUsuario SET 
						avatar = @avatar
							
					WHERE  cedula = @cedula 
	
	SELECT * FROM TbUsuario WHERE cedula = @cedula  
		
END
GO
/****** Object:  StoredProcedure [dbo].[editarUsuarioPass]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editarUsuarioPass]
(
	@cedula int,
	@contrasena varchar(20)
)

AS
BEGIN

UPDATE TbUsuario SET 
						contrasena = @contrasena
							
					WHERE  cedula = @cedula 
	
	SELECT * FROM TbUsuario WHERE cedula = @cedula  
		
END
GO
/****** Object:  StoredProcedure [dbo].[insertarActividad]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertarActividad] (
					@codigo varchar(10), 
					@nombre varchar(50),
					@descripcion varchar(100))AS
BEGIN
SET NOCOUNT ON
INSERT INTO TbActividad ( 
						codigo, 
						nombre,
						descripcion)
VALUES(
		@codigo,
		@nombre,
		@descripcion
		)
END
GO
/****** Object:  StoredProcedure [dbo].[insertarBitacora]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertarBitacora]
(
	@codigoProyecto varchar(10), 
	@proyecto varchar(50),
	@actividad varchar(50),
	@fechaIni date,
	@fechaFin date,
	@horaIni time,
	@horaFin time

)

AS
BEGIN
SET NOCOUNT ON
INSERT INTO TbBitacora
( 
	codigoProyecto, 
	proyecto,
	actividad,
	fechaInicio,
	fechaFin,
	horaInicio,
	horaFin
	)

VALUES(
		@codigoProyecto,
		@proyecto,
		@actividad,
		@fechaIni,
		@fechaFin,
		@horaIni,
		@horaFin
		)
END
GO
/****** Object:  StoredProcedure [dbo].[insertarClienteFisico]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertarClienteFisico]
(
	@id int, 
	@nombre varchar(50),
	@apellido1 varchar(50),
	@apellido2 varchar(50),
	@provincia varchar(50),
	@canton varchar(50),
	@distrito varchar(50),
	@direccion varchar(50),
	@email varchar(50),
	@telefono varchar(50)
)

AS
BEGIN
SET NOCOUNT ON
INSERT INTO TbClienteFi
( 
	cedula, 
	nombre,
	apellido1,
	apellido2,
	provincia,
	canton,
	distrito,
	direccion,
	email,
	telefono
	)

VALUES(
		@id, 
		@nombre,
		@apellido1,
		@apellido2,
		@provincia,
		@canton,
		@distrito,
		@direccion,
		@email,
		@telefono
		)
END
GO
/****** Object:  StoredProcedure [dbo].[insertarClienteJuridico]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertarClienteJuridico]
(
	@cedulaJuridica int,
	@nombreCliente varchar(50),
	@provincia varchar(50),
	@canton varchar(50),
	@distrito varchar(50),
	@direccion varchar(50),
	@cedula int, 
	@nombre varchar(50),
	@apellido1 varchar(50),
	@apellido2 varchar(50),
	@email varchar(50),
	@telefono varchar(50)
)

AS
BEGIN
SET NOCOUNT ON
INSERT INTO TbClienteJu
(	
	cedulaJuridica,
	nombreCliente,
	provincia,
	canton,
	distrito,
	direccion,
	cedula, 
	nombre,
	apellido1,
	apellido2,
	email,
	telefono
	)

VALUES(
		@cedulaJuridica,
		@nombreCliente,
		@provincia,
		@canton,
		@distrito,
		@direccion,
		@cedula, 
		@nombre,
		@apellido1,
		@apellido2,
		@email,
		@telefono
		)
END
GO
/****** Object:  StoredProcedure [dbo].[insertarProyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertarProyecto]
(
	@codigoProyecto varchar(10),
	@nombre varchar(50),
	@descripcion varchar(100),
	@fechaInicio date,
	@fechaFin date,
	@tipoProyecto varchar(50) ,
	@cliente varchar(100)
)

AS
BEGIN
SET NOCOUNT ON
INSERT INTO TbProyecto
(	
	codigoProyecto,
	nombre,
	descripcion,
	fechaInicio,
	fechaFin,
	tipoProyecto,
	cliente
	)

VALUES(
		@codigoProyecto,
		@nombre,
		@descripcion,
		@fechaInicio,
		@fechaFin,
		@tipoProyecto,
		@cliente
		)
END
GO
/****** Object:  StoredProcedure [dbo].[insertarReporte]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insertarReporte]
(
	@proyecto varchar(50),
	@actividad varchar(50),
	@tiempoInvertido varchar(20)

)

AS
BEGIN
SET NOCOUNT ON
INSERT INTO TbReporte
(	
	proyecto,
	actividad,
	tiempoInvertido
	)

VALUES(
		@proyecto,
		@actividad,
		@tiempoInvertido
		)
END
GO
/****** Object:  StoredProcedure [dbo].[insertarTecnologia]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertarTecnologia]
(
	@codigo varchar(10),
	@tecnologia varchar(50)
)

AS
BEGIN
SET NOCOUNT ON
INSERT INTO TbTecnologia
(	
	codigo,
	tecnologia
)

VALUES(
		@codigo,
		@tecnologia
		)
END
GO
/****** Object:  StoredProcedure [dbo].[insertarUsuario]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertarUsuario]
(
	@cedula int,
	@nombre varchar(50),
	@apellido1 varchar(50),
	@apellido2 varchar(50),
	@email varchar(50),
	@genero varchar(50),
	@avatar varchar(300),
	@contrasena varchar(20)

)

AS
BEGIN
SET NOCOUNT ON
INSERT INTO TbUsuario
(	
	cedula,
	nombre,
	apellido1,
	apellido2,
	email,
	genero,
	avatar,
	contrasena
)

VALUES(
		@cedula,
		@nombre,
		@apellido1,
		@apellido2,
		@email,
		@genero,
		@avatar,
		@contrasena
		)
END
GO
/****** Object:  StoredProcedure [dbo].[obtenerActividadesXproyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[obtenerActividadesXproyecto]
(
@codigoProyecto varchar(10)
)

AS
BEGIN

SET nocount on;

	SELECT A.nombre
      FROM TbActividadesXproyecto AxP INNER JOIN TbActividad A ON AxP.codigoActividades = A.codigo
      WHERE codigoProyecto = @codigoProyecto
  
END
GO
/****** Object:  StoredProcedure [dbo].[obtenerTecnologiasXproyecto]    Script Date: 8/19/2019 8:52:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[obtenerTecnologiasXproyecto]
(
@codigoProyecto varchar(10)
)

AS
BEGIN

SET nocount on;

	SELECT T.tecnologia
      FROM TbTecnologiasXproyecto TxP INNER JOIN TbTecnologia T ON TxP.codigoTecnologias = T.codigo
      WHERE codigoProyecto = @codigoProyecto
  
END
GO
USE [master]
GO
ALTER DATABASE [BDv_log] SET  READ_WRITE 
GO
