package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import cr.ac.ucenfotec.bl.dao.usuario.Usuario;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SettingsController extends MainController implements Initializable {


    @FXML
    private JFXButton loadImageBTN;
    @FXML
    private JFXPasswordField newPassTF;
    @FXML
    private JFXPasswordField passConfirmTF;
    @FXML
    private JFXButton updatePassBTN;
    @FXML
    private VBox avatarVbox;

    ImageView imgViewAvatar;
    String imagenSalvada = "";


    private void addElementstoGUI() throws Exception {

        imgViewAvatar = new ImageView();
        avatarVbox.getChildren().addAll(imgViewAvatar);

        Usuario u = obtenerUsuario();
        File tmpAvatar = new File(u.getAvatar());
        BufferedImage bufferedImage = ImageIO.read(tmpAvatar);
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        imgViewAvatar.setImage(image);
        imgViewAvatar.setFitHeight(450);
        imgViewAvatar.setFitWidth(450);
    }


    public void changePassword(ActionEvent event) throws Exception {

        //si alguno de los campos esta vacio, error
        if (newPassTF.getText().isEmpty() || passConfirmTF.getText().isEmpty()) {
            changePassInfoIncompleta(event);
        }
        //si los campos no coinciden
        else if (!newPassTF.getText().equals(passConfirmTF.getText())) {
            contrasenasNoCoinciden(event);
        } else {

            //obtener usuario
            Usuario user = new Usuario();
            for (Usuario tmpU : gt.obtenerUsuarios()) {
                user = tmpU;
            }

            //editar pass
            gt.editarUsuarioPass(user.getId(), passConfirmTF.getText());
            changePassCompleto(event);
        }
    }

    public void changeAvatar(ActionEvent event) throws Exception {


        //obtener nueva imagen
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG =
                new FileChooser.ExtensionFilter("JPG files (*.JPG)", "*.JPG");
        FileChooser.ExtensionFilter extFilterjpg =
                new FileChooser.ExtensionFilter("jpg files (*.jpg)", "*.jpg");
        FileChooser.ExtensionFilter extFilterPNG =
                new FileChooser.ExtensionFilter("PNG files (*.PNG)", "*.PNG");
        FileChooser.ExtensionFilter extFilterpng =
                new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
        fileChooser.getExtensionFilters()
                .addAll(extFilterJPG, extFilterjpg, extFilterPNG, extFilterpng);

        //Show open file dialog
        File file2 = fileChooser.showOpenDialog(null);

        try {
            BufferedImage bufferedImage = ImageIO.read(file2);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imgViewAvatar.setImage(image);
            imgViewAvatar.setFitHeight(450);
            imgViewAvatar.setFitWidth(450);

            imagenSalvada = file2.toString();


        } catch (IOException ex) {
            Logger.getLogger(SettingsController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //editar avatar
        Usuario u = obtenerUsuario();
        gt.editarUsuarioAvatar(u.getId(), imagenSalvada);
        changeAvatarCompleto(event);

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            addElementstoGUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            showMiniAvatar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
