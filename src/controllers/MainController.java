package controllers;


import cr.ac.ucenfotec.bl.dao.usuario.Usuario;
import com.jfoenix.controls.JFXButton;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import cr.ac.ucenfotec.tl.Controller;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.ResourceBundle;


public class MainController implements Initializable {


    public static Controller gt = new Controller();
    public static PrintStream out = System.out;


    //mini avatar
    @FXML
    private VBox miniAvatarVbox;
    private ImageView imgViewMiniAvatar;

    public Usuario obtenerUsuario() throws Exception {
        //obtener usuario
        Usuario user = new Usuario();
        for (Usuario tmpU : gt.obtenerUsuarios()) {
            user = tmpU;
        }
        return user;
    }


    //este metodo llama al url de la imagen registrada por el usuario, la convierte en imagen y
    //la muestra en todas las ventanas del programa. Si no se registro una imagen, se mantiene la default.

    public void showMiniAvatar() throws Exception {

        Usuario u = obtenerUsuario();
        imgViewMiniAvatar = new ImageView();

        //si la imagen existe
        if (!u.getAvatar().isEmpty()) {

            File file = new File(u.getAvatar());

            BufferedImage bufferedminiAvatar = ImageIO.read(file);
            Image miniAvatar = SwingFXUtils.toFXImage(bufferedminiAvatar, null);
            imgViewMiniAvatar.setImage(miniAvatar);
            imgViewMiniAvatar.setFitHeight(90);
            imgViewMiniAvatar.setFitWidth(90);

            miniAvatarVbox.getChildren().addAll(imgViewMiniAvatar);

        }
    }

    //=======================================================================================================//
    //METODOS PARA INGRESAR A LAS DIFERENTES VENTANAS
    //=======================================================================================================//

    //este metodo hace que el boton de TECNOLOGIAS se pase a vista TECNOLOGIAS
    public void changeScreenButtonTech(ActionEvent event) throws IOException {
        Parent techParent = FXMLLoader.load(getClass().getResource("../fxml/tech.fxml"));
        Scene techViewScene = new Scene(techParent);

        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de tecnologias
        window.setScene(techViewScene);
        window.show();
    }

    //este metodo hace que el boton de CLIENTES se pase a vista CLIENTES
    public void changeScreenButtonClientes(ActionEvent event) throws IOException {
        Parent clientParent = FXMLLoader.load(getClass().getResource("../fxml/clientes.fxml"));
        Scene clientViewScene = new Scene(clientParent);

        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de clientes
        window.setScene(clientViewScene);
        window.show();

    }

    //este metodo hace que el boton de PANEL/LOGO se pase a vista PANEL (home)
    public void changeScreenButtonPanel(ActionEvent event) throws IOException {
        Parent panelParent = FXMLLoader.load(getClass().getResource("../fxml/panel.fxml"));
        Scene panelViewScene = new Scene(panelParent);

        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de panel
        window.setScene(panelViewScene);
        window.show();

    }

    //este metodo hace que el boton de VER REPORTESse pase a vista REPORTES
    public void changeScreenButtonReportes(ActionEvent event) throws IOException {
        Parent panelParent = FXMLLoader.load(getClass().getResource("../fxml/reportes.fxml"));
        Scene panelViewScene = new Scene(panelParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de panel
        window.setScene(panelViewScene);
        window.show();

    }

    //este metodo hace que el boton de PROYECTOS se pase a vista PROYECTOS
    public void changeScreenButtonProyectos(ActionEvent event) throws IOException {
        Parent projParent = FXMLLoader.load(getClass().getResource("../fxml/proyectos.fxml"));
        Scene projViewScene = new Scene(projParent);


        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana proyectos
        window.setScene(projViewScene);
        window.show();

    }

    //este metodo hace que el boton de INFO se pase a vista INFO
    public void changeScreenButtonInfo(ActionEvent event) throws IOException {
        Parent profileParent = FXMLLoader.load(getClass().getResource("../fxml/info.fxml"));
        Scene profileViewScene = new Scene(profileParent);

        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de perfil/settings
        window.setScene(profileViewScene);
        window.show();
    }

    //este metodo hace que el boton de cliente fisico lo lleve al formulario
    public void changeScreenButtonClienteFisico(ActionEvent event) throws IOException {
        Parent profileParent = FXMLLoader.load(getClass().getResource("../fxml/clienteFisico.fxml"));
        Scene profileViewScene = new Scene(profileParent);

        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de perfil/settings
        window.setScene(profileViewScene);
        window.show();

    }

    //este metodo hace que el boton de cliente juridico lo lleve al formulario
    public void changeScreenButtonClienteJuridico(ActionEvent event) throws IOException {
        Parent profileParent = FXMLLoader.load(getClass().getResource("../fxml/clienteJuridico.fxml"));
        Scene profileViewScene = new Scene(profileParent);

        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de perfil/settings
        window.setScene(profileViewScene);
        window.show();

    }

    //este metodo hace que el boton de LOGOUT se pase a vista INICIAR SESION
    public void changeScreenButtonLogOut(ActionEvent event) throws IOException {
        Parent profileParent = FXMLLoader.load(getClass().getResource("../fxml/login.fxml"));
        Scene profileViewScene = new Scene(profileParent);

        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de perfil/settings
        window.setScene(profileViewScene);
        window.show();

    }

    //este metodo hace que el boton de SETTINGS se pase a vista SETTINGS
    public void changeScreenButtonSettings(ActionEvent event) throws IOException {
        Parent profileParent = FXMLLoader.load(getClass().getResource("../fxml/settings.fxml"));
        Scene profileViewScene = new Scene(profileParent);

        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de perfil/settings
        window.setScene(profileViewScene);
        window.show();

    }

    //este metodo hace que el boton de REGISTRAR USUARIO se pase a vista REGISTRAR USUARIO
    public void changeScreenButtonRegUser(ActionEvent event) throws IOException {
        Parent profileParent = FXMLLoader.load(getClass().getResource("../fxml/usuario.fxml"));
        Scene profileViewScene = new Scene(profileParent);

        //obtener la informacion del stage
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //saltar a la ventana de perfil/settings
        window.setScene(profileViewScene);
        window.show();

    }


    //=======================================================================================================//
// MODAL WINDOWS
//=======================================================================================================//
//metodo generico para cerrar ventana modal
    public void closeWindow(ActionEvent event) throws IOException {

        boolean cerrar = true;
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
    }

    public void infoIncompletaLogin(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalInfoIncompletaLogin.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void infoIncorrectaLogin(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalInfoIncorrectaLogin.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void changePassCompleto(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalChangePass.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void changeAvatarCompleto(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalChangeAvatar.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void changePassInfoIncompleta(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalPassInfoIncompleta.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void contrasenasNoCoinciden(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalPassNoCoinciden.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void infoIncompletaRegistro(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalInfoIncompleta.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();


        JFXButton x = new JFXButton();
        x.setDisableVisualFocus(true);
    }

    public void advertenciaMonoUsuario(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalMonoUsuario.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Advertencia!");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void validarBotonEditar(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalValidarEditar.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void registroConfirmado(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalRegistroConfirmado.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void usuarioRegistrado(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalRegistroUsuario.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }


    public void infoRepetida(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalInfoRepetida.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }


    //modal para la contrasenia olvidada
    public void forgotPassModal(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalForgotPass.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Recuperar Contraseña");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    //modal errores numericos
    public void numberExceptionModal(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalErrorNumerico.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("ERROR");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    //modal para el registro de actividad en bitacora, inicio del conteo.
    public void timerBitacora(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/modalTimerBitacora.fxml"));
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Registro de Actividad");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}


