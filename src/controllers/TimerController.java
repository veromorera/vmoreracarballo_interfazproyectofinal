package controllers;

import com.jfoenix.controls.JFXButton;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.net.URL;
import java.time.LocalTime;
import java.util.ResourceBundle;

public class TimerController implements Initializable {

    @FXML
    VBox vbox = new VBox();
    @FXML
    private JFXButton startBTN;
    @FXML
    private JFXButton pauseBTN;
    @FXML
    private JFXButton restartBTN;

    Text display ;
    int second;
    int time = 0;

    //esto permite obtener la duracion de la tarea con timer
    public LocalTime obtenerTiempoTarea() {
        LocalTime duracionDeTarea = LocalTime.parse(display.getText());
        return duracionDeTarea;
    }



    public VBox getStopWatch() {
        return vbox;
    }


    public void setTimer(int time)
    {
        display = new Text(String.format("%02d:%02d:%02d", second / 3600, (second % 3600) / 60, second % 60));

    }





    @Override
    public void initialize(URL location, ResourceBundle resources) {



        //crear un nuevo texto en formato 00:00:00
        display = new Text(String.format("%02d:%02d:%02d", second / 3600, (second % 3600) / 60, second % 60));

        //establecer el diseno de la fuente
        display.setFont(Font.font("Microsoft JhengHei Bold", 45));

//------------------------------------------------------------------------------------------------------------------//

        //hacer la animacion del tiempo, aplicarselo
        Timeline stopwatchTimeline = new Timeline(new KeyFrame(Duration.seconds(1), (ActionEvent event) -> {
            second++; //Si quisiera hacer un contador hacia abajo lo pongo en --
            display.setText(String.format("%02d:%02d:%02d", second / 3600, (second % 3600) / 60, second % 60));
        }));
        //establecer el tiempo indefinido para que el usuario elija cuando terminarlo.
        stopwatchTimeline.setCycleCount(Timeline.INDEFINITE);



//------------------------------------------------------------------------------------------------------------------//

        //establecer los eventos de los botones
        startBTN.setOnAction((event) ->{
            stopwatchTimeline.play();
            startBTN.setDisable(true);
            pauseBTN.setDisable(false);
            System.out.println("El temporizador a iniciado");

        });


        pauseBTN.setOnAction((event) -> {
            stopwatchTimeline.pause();
            startBTN.setDisable(false);
            pauseBTN.setDisable(true);
            System.out.println("El temporizador esta en pausa");

            //obtener el tiempo
            System.out.println("Tiempo en HH:MM:SS  ->  " + obtenerTiempoTarea());

        });
        restartBTN.setOnAction((event) -> {
            this.second = time * 60;
            display.setText(String.format("%02d:%02d:%02d", second / 3600, (second % 3600) / 60, second % 60));
        });

        //agregar el "texto" del tiempo en un vBox.
        vbox.getChildren().addAll(display);

        //definir estilos y posicion del vBox
        //vbox.setStyle("-fx-background-color: #336699;");


        //correr el timer.

        TimerController stopwatch;
        stopwatch = new TimerController();
        vbox.getChildren().addAll(stopwatch.getStopWatch());

    }
}

