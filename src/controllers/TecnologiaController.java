package controllers;

import com.jfoenix.controls.JFXTextField;
import cr.ac.ucenfotec.bl.dao.tecnologia.Tecnologia;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.util.Callback;

public class TecnologiaController extends MainController implements Initializable {


    @FXML
    private JFXTextField techBuscar;
    @FXML
    private JFXTextField techCodigoTF;
    @FXML
    private JFXTextField techNombreTF;
    @FXML
    private TableView<Tecnologia> techTableView;
    @FXML
    private TableColumn<Tecnologia, String> codigoCol;
    @FXML
    private TableColumn<Tecnologia, String> techCol;

    private ObservableList<Tecnologia> listTech;

//=======================================================================================================//
    //metodo para que se muestren los datos en la tabla (listar)
    private void showDetails(TreeItem<Tecnologia> treeItem) {

        techCodigoTF.setText(treeItem.getValue().getCodigo());
        techNombreTF.setText(treeItem.getValue().getNombre());
    }

//=======================================================================================================//
    //metodo para verificar la informacion
    public void verificarAgregar(ActionEvent event) throws Exception {

        //verificar si la info esta incompleta
        if (techCodigoTF.getText().isEmpty() || techNombreTF.getText().isEmpty()) {

            infoIncompletaRegistro(event);
        }
        else if (!gt.buscarTecnologia(techCodigoTF.getText(), techNombreTF.getText())) {

            agregarAction();
        } else infoRepetida(event);
    }
//=======================================================================================================//
    //metodo para insertar desde el inicio los datos en la tabla
    private void InsertarDatosTabla() throws Exception {

        for (Tecnologia tec : gt.obtenerTecnologias()) {

            //agregar lo que este en el arraylist a la tabla de tec
            listTech.addAll(tec);
        }
    }
//=======================================================================================================//
    @FXML
    private void agregarAction() throws Exception {

        //registrar tecnologia
        gt.registrarTecnologia(techCodigoTF.getText(), techNombreTF.getText());

        //agregarla a la lista "observable"
        listTech.addAll(new Tecnologia(techCodigoTF.getText(), techNombreTF.getText()));

        //limpiar formulario
        clearFields();
    }

//=======================================================================================================//
    @FXML
    public void editarAction(ActionEvent event) throws Exception {

        //seleccionar la info de la tabla.
        TreeItem<Tecnologia> treeItem = new TreeItem<>(techTableView.getSelectionModel().getSelectedItem());


        //validar si hay info seleccionada
        if (techTableView.getSelectionModel().getSelectedItem()== null) {
            validarBotonEditar(event);

        } else {

            //convertirla en tecnologia
            Tecnologia t = new Tecnologia(techCodigoTF.getText(), techNombreTF.getText());

            //editarla con el gestor
            gt.editarTecnologia(t.getCodigo(), t.getNombre());

            //volverla a asignar en la tabla.
            treeItem.setValue(t);

            //refrescar la pagina
            changeScreenButtonTech(event);
        }
    }
//=======================================================================================================//
    /*
    No es requisito borrar para este proyecto
    @FXML
    void  borrarAction(ActionEvent event){
        int index=treeTableView.getSelectionModel().getSelectedIndex();
        listTech.remove(index);
        clearFields();
    }
     */
//=======================================================================================================//
    //metodo para limpiar los campos del formulario.
    private void clearFields() {
        techCodigoTF.setText("");
        techNombreTF.setText("");
    }
//=======================================================================================================//
    @FXML
    public void limpiarAction(ActionEvent event) {
        clearFields();
    }


    //=======================================================================================================//
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            showMiniAvatar();
        } catch (Exception e) {
            e.printStackTrace();
        }


        //asignar valores a la columna CODIGO en la tabla
        codigoCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Tecnologia, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Tecnologia, String> param) {
                return new SimpleStringProperty(param.getValue().getCodigo());
            }
        });

        //asignar valores a la columna TECNOLOGIA en la tabla
        techCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Tecnologia, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Tecnologia, String> param) {
                return param.getValue().nombreProperty();
            }
        });


        //Hacer un arraylist de tipo "observable" para que pueda mostrar datos en la tabla.
        //Estas tecnologias igualmente se estan registrando con el gestor en el void "agregarAccion"
        listTech = FXCollections.observableArrayList();

        //Agregar los datos que ya estaban en el arraylist previamente
        try {
            InsertarDatosTabla();
        } catch (Exception e) {
            e.printStackTrace();
        }


//=======================================================================================================//

        //para editar, si se seleccionan datos de la tabla, pegarlos en el formulario
        techTableView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {

            if (techTableView.getSelectionModel().getSelectedItem() != null) {

                //el codigo no se puede editar,
                //esto es para que no hayan problemas de repeticion en la base de datos
                techCodigoTF.setEditable(false);
                techCodigoTF.setDisable(true);

                TreeItem tempTech = new TreeItem(newValue);
                showDetails(tempTech);
            }
        });

//=======================================================================================================//
        // Inicializar la columna nombre (el cliente se busca por nombre)
        techCol.setCellValueFactory(cellData -> cellData.getValue().nombreProperty());

        // Meter el arraylist en una lista filtrada
        FilteredList<Tecnologia> filteredData = new FilteredList<>(listTech, p -> true);

        //setear el filtro predictivo cada que cambie
        techBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(clienteFisico -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Comparar los nombre de la tabla con el de la busqueda
                String lowerCaseFilter = newValue.toLowerCase();

                if (clienteFisico.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                }
                return false; // Does not match.
            });
        });

        //convertir la lista filtrada en lista ordenada
        SortedList<Tecnologia> sortedData = new SortedList<>(filteredData);

        // hacer bind de la lista sorted en la tabla
        sortedData.comparatorProperty().bind(techTableView.comparatorProperty());

        // Agregar la lista sorted a la tabla.
        techTableView.setItems(sortedData);

    }
}