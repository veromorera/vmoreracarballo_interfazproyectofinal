package controllers;

import com.jfoenix.controls.*;
import cr.ac.ucenfotec.bl.dao.actividad.Actividad;
import cr.ac.ucenfotec.bl.dao.clientefisico.ClienteFisico;
import cr.ac.ucenfotec.bl.dao.clientejuridico.ClienteJuridico;
import cr.ac.ucenfotec.bl.dao.proyecto.Proyecto;
import cr.ac.ucenfotec.bl.dao.tecnologia.Tecnologia;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.util.Callback;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ProyectosController extends MainController implements Initializable {

    @FXML
    private JFXTextField projBuscar;
    @FXML
    private JFXTextField projCodigoTF;
    @FXML
    private JFXTextField projNombreTF;
    @FXML
    private JFXTextField projDescripcionTF;
    @FXML
    private JFXDatePicker projFechaInicioDP;
    @FXML
    private JFXDatePicker projFechaFinDP;
    @FXML
    private JFXTextField projCodigoActividadTF;
    @FXML
    private JFXTextField projNombreActividadTF;
    @FXML
    private JFXButton regActividadBTN;
    @FXML
    private JFXTextField projDescripcionActividadTF;
    @FXML
    private JFXComboBox<String> projClienteCombo;
    @FXML
    private JFXComboBox<String> projTecCombo;
    @FXML
    private JFXButton projRegTechBTN;
    @FXML
    private JFXComboBox<String> projTipoProyectoCombo;
    @FXML
    private TableView<Proyecto> projTableView;
    @FXML
    private TableColumn<Proyecto, String> codigoCol;
    @FXML
    private TableColumn<Proyecto, String> nombreCol;
    @FXML
    private TableColumn<Proyecto, String> descripCol;
    @FXML
    private TableColumn<Proyecto, LocalDate> fechaIniCol;
    @FXML
    private TableColumn<Proyecto, LocalDate> fechaFinCol;
    @FXML
    private TableColumn<Proyecto, ArrayList> techCol;
    @FXML
    private TableColumn<Proyecto, ArrayList> activCol;
    @FXML
    private TableColumn<Proyecto, String> tipoProjCol;
    @FXML
    private TableColumn<Proyecto, String> clienteCol;

    private ObservableList<Proyecto> listProj;


    //definir las opciones de tipo de proyecto
    private ObservableList<String> tipos = FXCollections.observableArrayList("Académico", "Comercial");

    //arrays para proyecto (lista de solo nombres)
    private ArrayList<String> tecnologiasArr = new ArrayList<>();
    private ArrayList<String> actividadesArr = new ArrayList<>();

    //tengo q hacer 2 arraylist para acumular los codigos de proyecto
    //para que se puedan asociar en base de datos
    ArrayList<String> acumuladorCodigoAct = new ArrayList<>();
    ArrayList<String> acumuladorCodigoTech = new ArrayList<>();


//=======================================================================================================//


    //metodo para que se muestren los datos en la tabla (listar)
    private void showDetails(TreeItem<Proyecto> treeItem) {

        projCodigoTF.setText(treeItem.getValue().getCodigo());
        projNombreTF.setText(treeItem.getValue().getNombreProyecto());
        projDescripcionTF.setText(treeItem.getValue().getDescripcion());
        projFechaInicioDP.setValue(treeItem.getValue().getFechaInicio());
        projFechaFinDP.setValue(treeItem.getValue().getFechaFin());
        /*OJO*/
        projTecCombo.getSelectionModel().select(String.valueOf(treeItem.getValue().getTecnologias()));
        //
        projTipoProyectoCombo.getSelectionModel().select(treeItem.getValue().getTipoProyecto());
        projClienteCombo.getSelectionModel().select(treeItem.getValue().getCliente());

    }

    //=======================================================================================================//
    //metodo para que se muestren los nombres de las ACTIVIDADES en la tabla (listar)
    private void showDetailsActiv(TreeItem<Actividad> treeItem) {

        projNombreActividadTF.setText(treeItem.getValue().getNombre());

    }

    //=======================================================================================================//
    //metodo para verificar informacion
    public void verificarAgregar(ActionEvent event) throws Exception {

        //verificar si la info esta incompleta
        if (projCodigoTF.getText().isEmpty() || projNombreTF.getText().isEmpty() ||
                projDescripcionTF.getText().isEmpty() || projFechaInicioDP.getValue() == null ||
                projFechaFinDP.getValue() == null || projTecCombo.getItems() == null ||
                projClienteCombo.getItems() == null || projTipoProyectoCombo.getItems() == null ||
                projCodigoActividadTF.getText().isEmpty() || projNombreActividadTF.getText().isEmpty() ||
                projDescripcionActividadTF.getText().isEmpty()) {

            infoIncompletaRegistro(event);
        }
        //verificar si ya existe
        else if (gt.buscarProyecto(projCodigoTF.getText(), projNombreTF.getText())) {

            infoRepetida(event);
        } else {
            agregarAction();
        }
    }

    //=======================================================================================================//
    //metodo para insertar los datos en la tabla
    private void InsertarDatosTabla() throws Exception {

        for (Proyecto proj : gt.obtenerProyectos()) {

            //agregar lo que este en el arraylist a la tabla de tec
            listProj.addAll(proj);
        }
    }

    //=======================================================================================================//
    @FXML
    private void agregarAction() throws Exception {

        //guarda actividades individualmente x proyecto, evita duplicados
        ArrayList<String> actividadesXproyecto = new ArrayList<>(actividadesArr);

        //registrar proyecto (despues se puede quitar el print, es solo para probar)
        gt.registrarProyecto(projCodigoTF.getText(), projNombreTF.getText(),
                projDescripcionTF.getText(), projFechaInicioDP.getValue(), projFechaFinDP.getValue(),
                tecnologiasArr, actividadesXproyecto, projClienteCombo.getSelectionModel().getSelectedItem(),
                projTipoProyectoCombo.getSelectionModel().getSelectedItem());


        //agregarla a la lista "observable"
        listProj.addAll(new Proyecto(projCodigoTF.getText(), projNombreTF.getText(), projDescripcionTF.getText(),
                projFechaInicioDP.getValue(), projFechaFinDP.getValue(),
                tecnologiasArr, actividadesXproyecto, projClienteCombo.getSelectionModel().getSelectedItem(),
                projTipoProyectoCombo.getSelectionModel().getSelectedItem()));


        //para cada codigo de actividad, asociarlo con el mismo proyecto
        for (String codigoActiv : acumuladorCodigoAct) {
            gt.asociarActividadesProyecto(codigoActiv, projCodigoTF.getText());
        }

        //para cada codigo de tech, asociarlo con el mismo proyecto
        for (String codigoTech : acumuladorCodigoTech) {
            gt.asociarTecnologiasProyecto(codigoTech, projCodigoTF.getText());
        }


        //limpiar formulario
        clearFields();

        //limpiar listas para evitar repetidos en otros proyectos.
        actividadesArr.clear();
        acumuladorCodigoAct.clear();
        acumuladorCodigoTech.clear();
    }

    //=======================================================================================================//
    @FXML
    public void editarAction(ActionEvent event) throws Exception {

        //seleccionar la info de la tabla.
        TreeItem<Proyecto> treeItem = new TreeItem<>(projTableView.getSelectionModel().getSelectedItem());


        //validar si hay info seleccionada
        if (projTableView.getSelectionModel().getSelectedItem() == null) {
            validarBotonEditar(event);

        } else {

            //guarda actividades individualmente x proyecto, evita duplicados
            ArrayList<String> actividadesXproyectoEdit = new ArrayList<>();

            //obtener proyecto seleccionado
            for (Proyecto projEdit : gt.findGetProyecto(projCodigoTF.getText())) {

                //obtener actividades de proyecto seleccionado
                for (Object activs : projEdit.getActividades()) {
                    actividadesXproyectoEdit.add(activs.toString());
                }
            }
            //agregar las actividades nuevas a las previas (en caso de que las haya):
            actividadesXproyectoEdit.addAll(actividadesArr);


            //asociar en base de datos
            //para cada codigo de actividad, asociarlo con el mismo proyecto
            for (String codigoActiv : acumuladorCodigoAct) {
                gt.asociarActividadesProyecto(codigoActiv, projCodigoTF.getText());
            }

            //para cada codigo de tech, asociarlo con el mismo proyecto
            for (String codigoTech : acumuladorCodigoTech) {
                gt.asociarTecnologiasProyecto(codigoTech, projCodigoTF.getText());
            }


            //convertir info en proyecto nuevamente
            Proyecto p = new Proyecto(projCodigoTF.getText(), projNombreTF.getText(), projDescripcionTF.getText(),
                    projFechaInicioDP.getValue(), projFechaFinDP.getValue(), tecnologiasArr,
                    actividadesXproyectoEdit, projClienteCombo.getSelectionModel().getSelectedItem(),
                    projTipoProyectoCombo.getSelectionModel().getSelectedItem());

            //editarla con el gestor
            gt.editarProyecto(p.getCodigo(), p.getNombreProyecto(), p.getDescripcion(),
                    p.getFechaInicio(), p.getFechaFin(),
                    p.getCliente(), p.getTipoProyecto());


            //volverla a asignar en la tabla.
            treeItem.setValue(p);

            //refrescar la pagina
            changeScreenButtonProyectos(event);
        }
    }


    //=======================================================================================================//
    //metodo para limpiar los campos del formulario.
    private void clearFields() {

        projBuscar.setText("");
        projCodigoTF.setText("");
        projNombreTF.setText("");
        projDescripcionTF.setText("");
        //
        projFechaInicioDP.setValue(null);
        projFechaFinDP.setValue(null);
        projTecCombo.setValue(null);
        projTipoProyectoCombo.setValue(null);
        projClienteCombo.setValue(null);
        //
        projCodigoActividadTF.setText("");
        projNombreActividadTF.setText("");
        projDescripcionActividadTF.setText("");
    }

    //=======================================================================================================//
    @FXML
    public void limpiarAction(ActionEvent event) {
        clearFields();
    }

    //=======================================================================================================//

    //metodo para registrar las actividades
    public void registrarActividades(ActionEvent event) throws Exception {

        //si la actividad ya existe
        if (gt.buscarActividadPorCodigo(projCodigoActividadTF.getText())) {

            infoRepetida(event);
        }

        //verificar si la info esta incompleta
        else if (projCodigoActividadTF.getText().isEmpty() ||
                projNombreActividadTF.getText().isEmpty() ||
                projDescripcionActividadTF.getText().isEmpty()) {

            infoIncompletaRegistro(event);

        } else {
            //registrar con el gestor
            gt.registrarActividad(projCodigoActividadTF.getText(),
                    projNombreActividadTF.getText(),
                    projDescripcionActividadTF.getText());


            //guardar solo los nombres para mostrarlos en tabla
            actividadesArr.add(projNombreActividadTF.getText());

            //guardar los codigos para asociarlos al proyecto en BD
            for (Actividad activ : gt.obtenerActividad()) {
                if (projCodigoActividadTF.getText().equals(activ.getCodigo())) {
                    acumuladorCodigoAct.add(activ.getCodigo());
                }
            }

            registroConfirmado(event);

        }
    }

    //=======================================================================================================//
    //este metodo extrae todas las tecnologias que hay registradas y las pone en la lista de seleccion
    //del proyecto, asi el usuario puede registrar las que necesita.

    private void llenarComboTecnologia() throws Exception {

        //lista observable para guardar lista de tecnologias (solo nombres)
        ObservableList<String> tech_ol = FXCollections.observableArrayList();


        //listar tecnologias existentes
        for (Tecnologia tec : gt.obtenerTecnologias()) {

            //extraer solo los nombres de las tecnologias
            String nombreTec = tec.nombreToString();

            //agregar los nombres a la lista
            tech_ol.add(nombreTec);

            //agregar esos valores al combo
            projTecCombo.setItems(tech_ol);
        }
    }

    //=======================================================================================================//
    //este metodo extrae todos los clientes que hay registrados y los pone en la lista de seleccion
    //del proyecto, asi el usuario puede registrar el que necesita.

    private void llenarComboClientes() throws Exception {

        //lista observable para guardar lista de clientes (solo nombres)
        ObservableList<String> cliente_ol = FXCollections.observableArrayList();

        //listar clientes fisicos existentes
        for (ClienteFisico cf : gt.obtenerClientesFi()) {

            //extraer solo los nombres
            String nombreCf = cf.nombreToString();

            //agregar los nombres a la lista
            cliente_ol.add(nombreCf);

            //agregar esos valores al combo
            projClienteCombo.setItems(cliente_ol);
        }

        //listar clientes juridicos existentes
        for (ClienteJuridico cj : gt.obtenerClientesJu()) {

            //extraer solo los nombres
            String nombreCj = cj.nombreToString();

            //agregar los nombres a la lista
            cliente_ol.add(nombreCj);

            //agregar esos valores al combo
            projClienteCombo.setItems(cliente_ol);
        }

    }


    public void onActionProperty() throws Exception {

        if (projTipoProyectoCombo.getSelectionModel().getSelectedItem().equals("Académico")) {

            //lista observable para guardar lista de clientes (solo nombres)
            ObservableList<String> user_ol = FXCollections.observableArrayList();

            //obtener usuario
            String usuarioNombreCompleto = obtenerUsuario().getNombre() + " " +
                    obtenerUsuario().getApellido1() + " " +
                    obtenerUsuario().getApellido2();

            //asignar lista observable al cliente combo
            user_ol.add(usuarioNombreCompleto);
            projClienteCombo.setItems(user_ol);
        } else {
            llenarComboClientes();
        }

    }


    //=======================================================================================================//
    //este metodo agrega las tecnologias al arraylist que posteriormente se registra en el proyecto

    public void agregarTecnologia(ActionEvent event) throws Exception {

        //verificar si la info esta incompleta
        if (projTecCombo.getSelectionModel().getSelectedItem() == null) {

            infoIncompletaRegistro(event);

        } else {
            //guardar solo los nombres para mostrarlos en tabla
            tecnologiasArr.add(projTecCombo.getSelectionModel().getSelectedItem());

            //guardar codigos para asociarlos a proyecto en BD
            for (Tecnologia tech : gt.obtenerTecnologias()) {
                if (projTecCombo.getSelectionModel().getSelectedItem().equals(tech.nombreToString())) {
                    acumuladorCodigoTech.add(tech.getCodigo());
                }
            }

            registroConfirmado(event);


        }





    }

//=======================================================================================================//

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            showMiniAvatar();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //agregar los tipos de proyecto al comboBox
        projTipoProyectoCombo.setItems(tipos);
        try {
            llenarComboTecnologia();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            llenarComboClientes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //limpiar lista de actividades para evistar repetidos en otros proyectos.
        actividadesArr.clear();


        //asignar valores a la columna CODIGO en la tabla
        codigoCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Proyecto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Proyecto, String> param) {
                return new SimpleStringProperty(param.getValue().getCodigo());
            }
        });

        //asignar valores a la columna NOMBRE PROYECTO en la tabla
        nombreCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Proyecto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Proyecto, String> param) {
                return param.getValue().nombreProyectoProperty();
            }
        });

        //asignar valores a la columna DESCRIPCION en la tabla
        descripCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Proyecto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Proyecto, String> param) {
                return param.getValue().descripcionProperty();
            }
        });

        //asignar valores a la columna FECHA INICIO en la tabla
        fechaIniCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Proyecto, LocalDate>, ObservableValue<LocalDate>>() {
            @Override
            public ObservableValue<LocalDate> call(TableColumn.CellDataFeatures<Proyecto, LocalDate> param) {
                return new SimpleObjectProperty<LocalDate>(param.getValue().getFechaInicio());
            }
        });

        //asignar valores a la columna FECHA FIN en la tabla
        fechaFinCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Proyecto, LocalDate>, ObservableValue<LocalDate>>() {
            @Override
            public ObservableValue<LocalDate> call(TableColumn.CellDataFeatures<Proyecto, LocalDate> param) {
                return new SimpleObjectProperty<LocalDate>(param.getValue().getFechaFin());
            }
        });

        //asignar valores a la columna TECNOLOGIAS en la tabla
        techCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Proyecto, ArrayList>, ObservableValue<ArrayList>>() {
            @Override
            public ObservableValue<ArrayList> call(TableColumn.CellDataFeatures<Proyecto, ArrayList> param) {
                return new SimpleObjectProperty<>(param.getValue().getTecnologias());
            }
        });

        //asignar valores a la columna ACTIVIDADES en la tabla
        activCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Proyecto, ArrayList>, ObservableValue<ArrayList>>() {
            @Override
            public ObservableValue<ArrayList> call(TableColumn.CellDataFeatures<Proyecto, ArrayList> param) {
                return new SimpleObjectProperty<>(param.getValue().getActividades());
            }
        });

        //asignar valores a la columna TIPO PROYECTO en la tabla
        tipoProjCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Proyecto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Proyecto, String> param) {
                return param.getValue().tipoProyectoProperty();
            }
        });

        //asignar valores a la columna CLIENTE en la tabla
        clienteCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Proyecto, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Proyecto, String> param) {
                return param.getValue().clienteProperty();
            }
        });


        //Hacer un arraylist de tipo "observable" para que pueda mostrar datos en la tabla.
        //Estas tecnologias igualmente se estan registrando con el gestor en el void "agregarAccion"
        listProj = FXCollections.observableArrayList();

        //Agregar los datos que ya estaban en el arraylist previamente
        try {
            InsertarDatosTabla();
        } catch (Exception e) {
            e.printStackTrace();
        }


        //=======================================================================================================//
        //para editar, si se seleccionan datos de la tabla, pegarlos en el formulario
        projTableView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {

            if (projTableView.getSelectionModel().getSelectedItem() != null) {

                //el codigo no se puede editar,
                //esto es para que no hayan problemas de repeticion en la base de datos
                projCodigoTF.setEditable(false);
                projCodigoTF.setDisable(true);

                TreeItem tempProj = new TreeItem(newValue);
                showDetails(tempProj);
            }
        });


//=======================================================================================================//

        // Inicializar la columna nombre (el cliente se busca por nombre)
        nombreCol.setCellValueFactory(cellData -> cellData.getValue().nombreProyectoProperty());

        // Meter el arraylist en una lista filtrada
        FilteredList<Proyecto> filteredData = new FilteredList<>(listProj, p -> true);

        //setear el filtro predictivo cada que cambie
        projBuscar.textProperty().addListener((observable, oldValue, newValue) -> {

            filteredData.setPredicate(Proyecto -> {
                // If filter text is empty, display all
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Comparar los nombre de la tabla con el de la busqueda
                String lowerCaseFilter = newValue.toLowerCase();

                return Proyecto.getNombreProyecto().toLowerCase().contains(lowerCaseFilter);
            });
        });

        //convertir la lista filtrada en lista ordenada
        SortedList<Proyecto> sortedData = new SortedList<>(filteredData);

        // hacer bind de la lista sorted en la tabla
        sortedData.comparatorProperty().bind(projTableView.comparatorProperty());

        // Agregar la lista sorted a la tabla.
        projTableView.setItems(sortedData);
    }

}