package controllers;

import cr.ac.ucenfotec.bl.dao.reporte.Reporte;
import cr.ac.ucenfotec.bl.dao.reporte.ReporteCurvaAp;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;

import java.net.URL;
import java.util.*;


public class ReportesController extends MainController implements Initializable {

    private ObservableList<Reporte> listRep;
    private ObservableList<ReporteCurvaAp> lisActivCurva;


    @FXML
    private TableView<Reporte> tiempoInvertidoTableView;
    @FXML
    private TableColumn<Reporte, String> proyectoCol;
    @FXML
    private TableColumn<Reporte, String> activCol;
    @FXML
    private TableColumn<Reporte, String> tiempoIvertidoCol;
    @FXML
    private TableView<ReporteCurvaAp> curvaApTableView;
    @FXML
    private TableColumn<ReporteCurvaAp, String> caActividadCol;
    @FXML
    private TableColumn<ReporteCurvaAp, String> caCurvaApCol;

//=======================================================================================================//
//importar los datos para reporte


    //metodo para insertar desde el inicio los datos en la tabla
    private void InsertarDatosTabla() throws Exception {

        for (Reporte rep : gt.obtenerReportes()) {

            //agregar lo que este en el arraylist a la tabla
            listRep.addAll(rep);

        }
        //mostrar reporte
        reporteCurvaAprendizaje();
    }


    private void reporteCurvaAprendizaje() throws Exception {

        ArrayList<String> tiempoParaSumar = new ArrayList<>();
        ArrayList<String> tiempoSumado = new ArrayList<>();
        ArrayList<String> actividadesSinDuplicados = new ArrayList<>();


        //hacer un map que contenga por cada actividad el tiempo invertido a modo de lista
        Map<String, List<String>> map = new HashMap<String, List<String>>();

        for (Reporte rep : gt.obtenerReportes()) {
            String key = rep.getActividad();
            //si hay duplicadas:
            if (map.containsKey(key)) {
                List<String> list = map.get(key);
                list.add(rep.toString());

            } else {
                //las que no estan duplicadas tambien
                List<String> list = new ArrayList<String>();
                list.add(rep.toString());
                map.put(key, list);
            }
        }

        //agregar a un nuevo arraylist que contenga las actividades sin duplicados
        for (String mis_actividades : map.keySet()){
            actividadesSinDuplicados.add(mis_actividades) ;
        }

        //estos son los valores de tiempo invertido (en forma de lista)
        for (List tiempos : map.values()) {

            //estos son los valores sueltos de tiempo x actividad.
            for (Object t : tiempos) {

                //agregar los valores sueltos a un arraylist temporal
                tiempoParaSumar.add(t.toString());
            }

            //calcular el tiempo promedio por actividad y agregar a un arraylist.
            tiempoSumado.add(obtenerTiempoCurva(tiempoParaSumar));

            //limpiar el arraylist para que se sume cada grupo de actividades, no todas juntas.
            tiempoParaSumar.clear();

        }

       // out.println(actividadesSinDuplicados);
        //out.println(tiempoSumado);

        //convertir estos valores en clases (es necesario para poderlos listar en tabla)
        for (int i=0; i<actividadesSinDuplicados.size(); i++)

            lisActivCurva.add( new ReporteCurvaAp(actividadesSinDuplicados.get(i), tiempoSumado.get(i)));
    }



    //=======================================================================================================//
    //metodo para convertir los strings de tiempo invertido en tiempo y hacer un promedio
    private String obtenerTiempoCurva(ArrayList<String> tiemposXActividad) {

        ArrayList<String> timestampsList = new ArrayList<>();

        int cantidadActividades = tiemposXActividad.size();
        for (String cadaTiempo : tiemposXActividad) {
            timestampsList.add(cadaTiempo);
        }

        //sumar los tiempos
        long tm = 0;
        for (String tmp : timestampsList) {
            String[] arr = tmp.split(":");
            tm += Integer.parseInt(arr[2]);
            tm += 60 * Integer.parseInt(arr[1]);
            tm += 3600 * Integer.parseInt(arr[0]);
        }

        //obtener horas, min, seg
        long hh = tm / 3600;
        tm %= 3600;
        long mm = tm / 60;
        tm %= 60;
        long ss = tm;

        //sacarPromedio
        long hhProm = hh / cantidadActividades;
        long mmProm = mm / cantidadActividades;
        long ssProm = ss / cantidadActividades;

        //out.println(cantidadActividades);


        String tiempoCurva = format(hhProm) + ":" + format(mmProm) + ":" + format(ssProm);
        return tiempoCurva;
    }


    //metodo para formatear el tiempo invertido en curva de aprendizaje.
    private String format(long s) {
        if (s < 10) return "0" + s;
        else return "" + s;
    }

    //=======================================================================================================//


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            showMiniAvatar();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //asignar valores a la columna PROYECTO en la tabla
        proyectoCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Reporte, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Reporte, String> param) {
                return param.getValue().proyectoProperty();
            }
        });

        //asignar valores a la columna ACTIVIDAD en la tabla
        activCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Reporte, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Reporte, String> param) {
                return param.getValue().actividadProperty();
            }
        });

        //asignar valores a la columna TIEMPO INVERTIDO en la tabla
        tiempoIvertidoCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Reporte, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Reporte, String> param) {
                return param.getValue().tiempoInvertidoProperty();
            }
        });
        //=======================================================================================================//



        //asignar valores a la columna ACTIVIDAD (CurvaAprendizaje)
        caActividadCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReporteCurvaAp, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ReporteCurvaAp, String> param) {
                return param.getValue().actividadProperty();
            }
        });

        //asignar valores a la columna ACTIVIDAD (CurvaAprendizaje)
        caCurvaApCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReporteCurvaAp, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ReporteCurvaAp, String> param) {
                return param.getValue().tiempoPromedioProperty();
            }
        });



        //=======================================================================================================//



        //Hacer un arraylist de tipo "observable" para que pueda mostrar datos en la tabla.
        //Estas tecnologias igualmente se estan registrando con el gestor en el void "agregarAccion"
        listRep = FXCollections.observableArrayList();
        lisActivCurva = FXCollections.observableArrayList();


        //Agregar los datos que ya estaban en el arraylist previamente
        try {
            InsertarDatosTabla();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Meter el arraylist en una lista filtrada
        FilteredList<Reporte> filteredData = new FilteredList<>(listRep, p -> true);
        FilteredList<ReporteCurvaAp> filteredData2 = new FilteredList<>(lisActivCurva, p -> true);


        //convertir la lista filtrada en lista ordenada
        SortedList<Reporte> sortedData = new SortedList<>(filteredData);
        SortedList<ReporteCurvaAp> sortedData2 = new SortedList<>(filteredData2);


        // hacer bind de la lista sorted en la tabla
        sortedData.comparatorProperty().bind(tiempoInvertidoTableView.comparatorProperty());
        sortedData2.comparatorProperty().bind(curvaApTableView.comparatorProperty());


        // Agregar la lista sorted a la tabla.
        tiempoInvertidoTableView.setItems(sortedData);
        curvaApTableView.setItems(sortedData2);

    }
}
