package controllers;

import cr.ac.ucenfotec.bl.dao.bitacora.Bitacora;
import cr.ac.ucenfotec.bl.dao.proyecto.Proyecto;
import com.jfoenix.controls.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.Duration;

import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class PanelController extends MainController implements Initializable {

    @FXML
    private VBox miniAvatarVbox;
    ImageView imgViewMiniAvatar;

    @FXML
    private JFXTimePicker bitHoraFinTP;
    @FXML
    private JFXTimePicker bitHoraIniTP;
    @FXML
    private JFXDatePicker bitFechaIniDP;
    @FXML
    private JFXDatePicker bitFechaFinDP;
    @FXML
    private JFXComboBox<String> bitActividadesCombo;
    @FXML
    private JFXComboBox<String> bitCodigoProyectoCombo;
    @FXML
    private JFXTextField bitProyectoTF;
    @FXML
    private TableView<Bitacora> bitTableView;
    @FXML
    private TableColumn<Bitacora, String> projCol;
    @FXML
    private TableColumn<Bitacora, String> activCol;
    @FXML
    private TableColumn<Bitacora, LocalDate> fechaIniCol;
    @FXML
    private TableColumn<Bitacora, LocalDate> fechaFinCol;
    @FXML
    private TableColumn<Bitacora, LocalTime> horaIniCol;
    @FXML
    private TableColumn<Bitacora, LocalTime> horaFinCol;

    private ObservableList<Bitacora> listBitacora;


//=======================================================================================================//
    //timer
//=======================================================================================================//

    @FXML
    private VBox vbox = new VBox();
    @FXML
    private JFXButton startBTN;
    @FXML
    private JFXButton pauseBTN;
    @FXML
    private JFXButton restartBTN;

    private Text display;
    private int second;
    private int time = 0;

    //inicializar el timer
    //Nota: lo estoy inicializando por fuera para que no cargue todos los metodos del panel.
    //pero se podria hacer desde este mismo controller.
    private TimerController stopwatch = new TimerController();

    //esto permite obtener la duracion de la tarea con timer
    public LocalTime obtenerTiempoTarea() {

        return LocalTime.parse(display.getText());
    }

    //esto muestra la impresion del tiempo en el app
    public VBox getStopWatch() {
        return vbox;
    }

    //formatea el tiempo en 00:00:00
    public void setTimer(int time) {
        display = new Text(String.format("%02d:%02d:%02d", second / 3600, (second % 3600) / 60, second % 60));

    }


//=======================================================================================================//
//bitacora
//=======================================================================================================//

    //metodo para que se muestren los datos en la tabla (listar)
    private void showDetails(TreeItem<Bitacora> treeItem) {

        bitProyectoTF.setText(treeItem.getValue().getProyecto());
        bitActividadesCombo.getSelectionModel().select(treeItem.getValue().getActividad());
        bitFechaIniDP.setValue(treeItem.getValue().getFechaInicio());
        bitFechaIniDP.setValue(treeItem.getValue().getFechaInicio());
        bitFechaFinDP.setValue(treeItem.getValue().getFechaFin());
        bitHoraIniTP.setValue(treeItem.getValue().getHoraInicio());
        bitHoraFinTP.setValue(treeItem.getValue().getHoraFin());
    }

    //=======================================================================================================//

    //metodo para verificar la informacion
    public void verificarAgregar(ActionEvent event) throws Exception {

        boolean existeActividad = false;

        for (Bitacora bit : gt.obtenerBitacora()) {

            if (bit.getCodigoProyecto().equals(bitCodigoProyectoCombo.getSelectionModel().getSelectedItem()) &&
                    bit.getActividad().equals(bitActividadesCombo.getSelectionModel().getSelectedItem())) {

                //si esto se cumple, la actividad de ese proyecto ya esta registrada.
                existeActividad = true;
            }
        }

        //verificar si la info esta incompleta
        if (bitProyectoTF.getText().isEmpty() ||
                bitActividadesCombo.getSelectionModel().getSelectedItem() == null ||
                bitFechaIniDP.getValue() == null || bitFechaFinDP.getValue() == null ||
                bitHoraIniTP.getValue() == null) {

            infoIncompletaRegistro(event);
        }
        //aqui lo que se verifica es que no se repita una misma actividad por proyecto en la bitacora:
        else if (existeActividad) {

            infoRepetida(event);

        } else {
            //obtener hora finalizacion
            obtenerHoraFinalizacion();

            //agregar a bitacora
            agregarAction();

            //dar un mensaje de confirmacion (este es especial de la bitacora)
            timerBitacora(event);

            //generar reporte
            generarReporte();

            //refrescar la pagina
            changeScreenButtonPanel(event);

            //limpiar formulario
            clearFields();

        }
    }

    //=======================================================================================================//
    //metodo para insertar desde el inicio los datos en la tabla
    private void InsertarDatosTabla() throws Exception {

        for (Bitacora bit : gt.obtenerBitacora()) {

            //agregar lo que este en el arraylist a la tabla de tec
            listBitacora.addAll(bit);
        }
    }

    //=======================================================================================================//
    @FXML
    private void agregarAction() throws Exception {

        //registrar bitacora (despues se puede quitar el print, es solo para probar)
        gt.registrarBitacora(bitCodigoProyectoCombo.getSelectionModel().getSelectedItem(),
                bitProyectoTF.getText(), bitActividadesCombo.getSelectionModel().getSelectedItem(),
                bitFechaIniDP.getValue(), bitFechaFinDP.getValue(),
                bitHoraIniTP.getValue(), bitHoraFinTP.getValue());

        //agregarla a la lista "observable"
        listBitacora.addAll(new Bitacora(bitCodigoProyectoCombo.getSelectionModel().getSelectedItem(),
                bitProyectoTF.getText(), bitActividadesCombo.getSelectionModel().getSelectedItem(),
                bitFechaIniDP.getValue(), bitFechaFinDP.getValue(),
                bitHoraIniTP.getValue(), bitHoraFinTP.getValue()
        ));

    }

    //=======================================================================================================//
    //metodo para limpiar los campos del formulario.
    private void clearFields() {
        bitFechaIniDP.setValue(null);
        bitFechaFinDP.setValue(null);
        bitHoraIniTP.setValue(null);
        bitHoraFinTP.setValue(null);
        bitProyectoTF.setText("");
        bitActividadesCombo.setValue(null);
        bitCodigoProyectoCombo.setValue(null);
    }

    //=======================================================================================================//
    //metodo para obtener los proyectos existentes x codigo

    private void llenarInfoProyecto() throws Exception {

        //lista observable para guardar lista de clientes (solo nombres)
        ObservableList<String> projCod_ol = FXCollections.observableArrayList();

        //listar proyectos existentes
        for (Proyecto p : gt.obtenerProyectos()) {

            //extraer codigo
            String codigoProyecto = p.codigoProyectoToString();

            //agregar los codigos a la lista
            projCod_ol.add(codigoProyecto);

            //agregar los valores a cada combo
            bitCodigoProyectoCombo.setItems(projCod_ol);
        }
    }

    //=======================================================================================================//
    public void llenarCamposBitacora(ActionEvent event) throws Exception {

        ObservableList<String> projActiv_ol = FXCollections.observableArrayList();

        if (bitCodigoProyectoCombo.getSelectionModel().getSelectedItem() != null) {

            //para el proyecto seleccionado, extrer la info extra
            String codigo = bitCodigoProyectoCombo.getSelectionModel().getSelectedItem();

            for (Proyecto px : gt.findGetProyecto(codigo)) {
                //obtener nombre
                String nombreProyecto = px.nombreProyectoToString();

                //extraer actividades
                ArrayList<String> actividadesDelProyecto = px.getActividades();

                //extraer fechas de inicio y final
                LocalDate fechaIni = px.getFechaInicio();
                LocalDate fechaFin = px.getFechaFin();

                //agregar actividades una por una:
                for (String activ : actividadesDelProyecto) {
                    projActiv_ol.add(activ);
                    bitActividadesCombo.setItems(projActiv_ol);
                }

                bitProyectoTF.setText(nombreProyecto);
                bitFechaIniDP.setValue(fechaIni);
                bitFechaFinDP.setValue(fechaFin);

            }
        }


    }

    //=======================================================================================================//

    //metodo para sumar las horas de inicio y el tiempo invertido y obtener la de finalizacion

    private void obtenerHoraFinalizacion() throws ParseException {

        //hacer un arraylist con los localTime
        List<String> timestampsList = new ArrayList<String>();

        //sumar 00 y hora de fin del temporizador
        timestampsList.add("00:00:00");

        if (obtenerTiempoTarea() == LocalTime.parse("00:00")) {
            timestampsList.add("00:00:00");
        } else {
            timestampsList.add(obtenerTiempoTarea().toString());


            //formatear con calendar
            final DateFormat dt = new SimpleDateFormat("HH:mm:ss");
            final Calendar c = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
            long milliseconds = 0;
            c.clear();

            //convertir a numeros y sumar
            long startingMS = c.getTimeInMillis();
            for (final String t : timestampsList) {
                milliseconds = milliseconds + (dt.parse(t).getTime() - startingMS);
            }
            long horaLong = (milliseconds / 1000 / 60 / 60);
            int hora = Math.toIntExact(horaLong);
            long minLong = (milliseconds / 1000 / 60);
            int min = Math.toIntExact(minLong);

            //calcular la fecha de finalizacion
            LocalTime timeTemp = LocalTime.of(hora, min);
            Integer hourTemp = timeTemp.getHour();
            Integer minuteTemp = timeTemp.getMinute();

            //sumar a la hora y a los minutos el tiempo del timer
            bitHoraFinTP.setValue(bitHoraIniTP.getValue().plusHours(hourTemp));
            bitHoraFinTP.setValue(bitHoraIniTP.getValue().plusMinutes(minuteTemp));
        }
    }

    //=======================================================================================================//
    //metodo para generar reporte
    private void generarReporte() throws Exception {

        String proyecto = "";
        String actividad = "";
        String tiempoInvertido = obtenerTiempoTarea().toString();

        for (Bitacora bit : gt.obtenerBitacora()) {
            proyecto = bit.getProyecto();
        }

        for (Bitacora bit : gt.obtenerBitacora()) {
            actividad = bit.getActividad();
        }

        gt.registrarReporte(proyecto, actividad, tiempoInvertido);
    }


    //=======================================================================================================//

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            obtenerUsuario();
            showMiniAvatar();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //obtener info de proyectos existentes automaticamente
        try {
            llenarInfoProyecto();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //asignar valores a la columna CODIGO en la tabla
        projCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bitacora, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Bitacora, String> param) {
                return param.getValue().proyectoProperty();
            }
        });

        //asignar valores a la columna ACTIVIDAD en la tabla
        activCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bitacora, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Bitacora, String> param) {
                return param.getValue().actividadProperty();
            }
        });

        //asignar valores a la columna FECHA INICIO en la tabla
        fechaIniCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bitacora, LocalDate>, ObservableValue<LocalDate>>() {
            @Override
            public ObservableValue<LocalDate> call(TableColumn.CellDataFeatures<Bitacora, LocalDate> param) {
                return new SimpleObjectProperty<LocalDate>(param.getValue().getFechaInicio());
            }
        });
        //asignar valores a la columna FECHA INICIO en la tabla
        fechaFinCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bitacora, LocalDate>, ObservableValue<LocalDate>>() {
            @Override
            public ObservableValue<LocalDate> call(TableColumn.CellDataFeatures<Bitacora, LocalDate> param) {
                return new SimpleObjectProperty<LocalDate>(param.getValue().getFechaFin());
            }
        });

        //asignar valores a la columna HORA INICIO en la tabla
        horaIniCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bitacora, LocalTime>, ObservableValue<LocalTime>>() {
            @Override
            public ObservableValue<LocalTime> call(TableColumn.CellDataFeatures<Bitacora, LocalTime> param) {
                return new SimpleObjectProperty<LocalTime>(param.getValue().getHoraInicio());
            }
        });

        //asignar valores a la columna HORA INICIO en la tabla
        horaFinCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bitacora, LocalTime>, ObservableValue<LocalTime>>() {
            @Override
            public ObservableValue<LocalTime> call(TableColumn.CellDataFeatures<Bitacora, LocalTime> param) {
                return new SimpleObjectProperty<LocalTime>(param.getValue().getHoraFin());
            }
        });


        //=======================================================================================================//
        //Hacer un arraylist de tipo "observable" para que pueda mostrar datos en la tabla.
        //Estas tecnologias igualmente se estan registrando con el gestor en el void "agregarAccion"
        listBitacora = FXCollections.observableArrayList();

        //Agregar los datos que ya estaban en el arraylist previamente
        try {
            InsertarDatosTabla();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //=======================================================================================================//

        // Meter el arraylist en una lista filtrada
        FilteredList<Bitacora> filteredData = new FilteredList<>(listBitacora, p -> true);


        //convertir la lista filtrada en lista ordenada
        SortedList<Bitacora> sortedData = new SortedList<>(filteredData);

        // hacer bind de la lista sorted en la tabla
        sortedData.comparatorProperty().bind(bitTableView.comparatorProperty());

        // Agregar la lista sorted a la tabla.
        bitTableView.setItems(sortedData);


//=======================================================================================================//
        //TIMER
//=======================================================================================================//

        //crear un nuevo texto en formato 00:00:00
        display = new Text(String.format("%02d:%02d:%02d", second / 3600, (second % 3600) / 60, second % 60));

        //establecer el diseno de la fuente
        display.setFont(Font.font("Microsoft JhengHei Bold", 45));

//------------------------------------------------------------------------------------------------------------------//

        //hacer la animacion del tiempo, aplicarselo
        Timeline stopwatchTimeline = new Timeline(new KeyFrame(Duration.seconds(1), (ActionEvent event) -> {
            second++; //Si quisiera hacer un contador hacia abajo lo pongo en --
            display.setText(String.format("%02d:%02d:%02d", second / 3600, (second % 3600) / 60, second % 60));
        }));
        //establecer el tiempo indefinido para que el usuario elija cuando terminarlo.
        stopwatchTimeline.setCycleCount(Timeline.INDEFINITE);


//------------------------------------------------------------------------------------------------------------------//

        //establecer los eventos de los botones
        startBTN.setOnAction((event) -> {
            stopwatchTimeline.play();
            startBTN.setDisable(true);
            pauseBTN.setDisable(false);
        });


        pauseBTN.setOnAction((event) -> {
            stopwatchTimeline.pause();
            startBTN.setDisable(false);
            pauseBTN.setDisable(true);
            System.out.println("El temporizador esta en pausa");

            //obtener el tiempo
            System.out.println("Tiempo en HH:MM:SS  ->  " + obtenerTiempoTarea());

        });
        restartBTN.setOnAction((event) -> {
            this.second = time * 60;
            display.setText(String.format("%02d:%02d:%02d", second / 3600, (second % 3600) / 60, second % 60));
        });

        //agregar el "texto" del tiempo en un vBox.
        vbox.getChildren().addAll(display);


        //correr el temporizador.
        vbox.getChildren().addAll(stopwatch.getStopWatch());


    }
}

