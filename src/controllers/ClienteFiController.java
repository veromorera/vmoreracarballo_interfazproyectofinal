package controllers;

import cr.ac.ucenfotec.bl.dao.clientefisico.ClienteFisico;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;


public class ClienteFiController extends MainController implements Initializable  {


    @FXML
    private JFXTextField clientFiBuscar;
    @FXML
    private JFXTextField clientFiNombreTF;
    @FXML
    private JFXTextField clientFiPriApellidoTF;
    @FXML
    private JFXTextField clientFiSegApellidoTF;
    @FXML
    private JFXTextField clientFiProvinciaTF;
    @FXML
    private JFXTextField clientFiCantonTF;
    @FXML
    private JFXTextField clientFiDistritoTF;
    @FXML
    private JFXTextField clientFiDireccionTF;
    @FXML
    private JFXTextField clientFiEmailTF;
    @FXML
    private JFXTextField clientFiTelTF;
    @FXML
    private JFXTextField clientFiIdTF;
    @FXML
    private TableView<ClienteFisico> clientFiTableView;
    @FXML
    private TableColumn<ClienteFisico, String> IdCol;
    @FXML
    private TableColumn<ClienteFisico, String> nombreCol;
    @FXML
    private TableColumn<ClienteFisico, String> apellido1Col;
    @FXML
    private TableColumn<ClienteFisico, String> apellido2Col;
    @FXML
    private TableColumn<ClienteFisico, String> provinciaCol;
    @FXML
    private TableColumn<ClienteFisico, String> cantonCol;
    @FXML
    private TableColumn<ClienteFisico, String> distritoCol;
    @FXML
    private TableColumn<ClienteFisico, String> direccionCol;
    @FXML
    private TableColumn<ClienteFisico, String> emailCol;
    @FXML
    private TableColumn<ClienteFisico, String> telCol;

    private ObservableList<ClienteFisico> listClienteFi;

    ImageView imgViewMiniAvatar;

    //=======================================================================================================//
//metodo para que se muestren los datos en la tabla (listar)
    private void showDetails(TreeItem<ClienteFisico> treeItem) {

    //convertir cedula a numero para llenar formulario
    String id = Integer.toString(treeItem.getValue().getId());

    clientFiIdTF.setText(id);
    clientFiNombreTF.setText(treeItem.getValue().getNombre());
    clientFiPriApellidoTF.setText(treeItem.getValue().getApellido1());
    clientFiSegApellidoTF.setText(treeItem.getValue().getApellido2());
    clientFiProvinciaTF.setText(treeItem.getValue().getProvincia());
    clientFiCantonTF.setText(treeItem.getValue().getCanton());
    clientFiDistritoTF.setText(treeItem.getValue().getDistrito());
    clientFiDireccionTF.setText(treeItem.getValue().getDireccion());
    clientFiEmailTF.setText(treeItem.getValue().getEmail());
    clientFiTelTF.setText(treeItem.getValue().getTelefono());
}

//=======================================================================================================//
    //metodo para verificar la informacion
    public void verificarAgregar(ActionEvent event) throws Exception {

    //verificar que la cedula sea un valor numerico.
    try{
        Integer.parseInt(clientFiIdTF.getText());

        //verificar si la info esta incompleta
        if (clientFiIdTF.getText().isEmpty() || clientFiNombreTF.getText().isEmpty() ||
                clientFiPriApellidoTF.getText().isEmpty() || clientFiSegApellidoTF.getText().isEmpty() ||
                clientFiProvinciaTF.getText().isEmpty() || clientFiCantonTF.getText().isEmpty() ||
                clientFiDistritoTF.getText().isEmpty() || clientFiDireccionTF.getText().isEmpty() ||
                clientFiEmailTF.getText().isEmpty() || clientFiTelTF.getText().isEmpty()){

            infoIncompletaRegistro(event);

        }//verificar si la info ya esta registrada
        else if (gt.buscarClienteFi(Integer.parseInt(clientFiIdTF.getText()))) {

            infoRepetida(event);
        } else {

            agregarAction();
        }
    }
    //obtener error en consola, imprimir mensaje para usuario
    catch (NumberFormatException e){
        out.println(e.getMessage());
        numberExceptionModal(event);
    }
}



    //=======================================================================================================//
    //metodo para insertar desde el inicio los datos en la tabla
    private void InsertarDatosTabla() throws Exception {

        for (ClienteFisico clientFi : gt.obtenerClientesFi()) {

            //agregar lo que este en el arraylist a la tabla de tec
            listClienteFi.addAll(clientFi);
        }
    }
    //=======================================================================================================//
    @FXML
    private void agregarAction() throws Exception {

    int id = Integer.parseInt(clientFiIdTF.getText());

        //registrar cliente (despues se puede quitar el print, es solo para probar)
        gt.registrarClienteFi(id, clientFiNombreTF.getText(), clientFiPriApellidoTF.getText(),
                                        clientFiSegApellidoTF.getText(), clientFiProvinciaTF.getText(),
                                        clientFiCantonTF.getText(), clientFiDistritoTF.getText(),
                                        clientFiDireccionTF.getText(),clientFiEmailTF.getText(),
                                        clientFiTelTF.getText());

        //agregarla a la lista "observable"
        listClienteFi.addAll(new ClienteFisico(Integer.parseInt(clientFiIdTF.getText()), clientFiNombreTF.getText(),
                                                clientFiPriApellidoTF.getText(), clientFiSegApellidoTF.getText(),
                                                clientFiProvinciaTF.getText(), clientFiCantonTF.getText(),
                                                clientFiDistritoTF.getText(), clientFiDireccionTF.getText(),
                                                clientFiEmailTF.getText(), clientFiTelTF.getText()));

        //limpiar formulario
        clearFields();
    }

    //=======================================================================================================//
    @FXML
    public void editarAction(ActionEvent event) throws Exception {


        //seleccionar la info de la tabla.
        TreeItem<ClienteFisico> treeItem = new TreeItem<>(clientFiTableView.getSelectionModel().getSelectedItem());


        //validar si hay info seleccionada
        if (clientFiTableView.getSelectionModel().getSelectedItem()== null) {
            validarBotonEditar(event);

        } else {
            //convertirla en cliente
            ClienteFisico cf = new ClienteFisico(Integer.parseInt(clientFiIdTF.getText()), clientFiNombreTF.getText(),
                    clientFiPriApellidoTF.getText(), clientFiSegApellidoTF.getText(),clientFiProvinciaTF.getText(),
                    clientFiCantonTF.getText(), clientFiDistritoTF.getText(), clientFiDireccionTF.getText(),
                    clientFiEmailTF.getText(), clientFiTelTF.getText());

            //editarla con el gestor
            gt.editarClienteFi(cf.getId(), cf.getNombre(), cf.getApellido1(), cf.getApellido2(),
                                cf.getProvincia(), cf.getCanton(), cf.getDistrito(), cf.getDireccion(),
                                cf.getEmail(), cf.getTelefono());


            //volverla a asignar en la tabla.
            treeItem.setValue(cf);

            //refrescar la pagina
            changeScreenButtonClienteFisico(event);

        }
    }
    //=======================================================================================================//
    //metodo para limpiar los campos del formulario.
    private void clearFields() {
        clientFiIdTF.setText("");
        clientFiNombreTF.setText("");
        clientFiPriApellidoTF.setText("");
        clientFiSegApellidoTF.setText("");
        clientFiProvinciaTF.setText("");
        clientFiCantonTF.setText("");
        clientFiDistritoTF.setText("");
        clientFiDireccionTF.setText("");
        clientFiEmailTF.setText("");
        clientFiTelTF.setText("");
    }
    //=======================================================================================================//
    @FXML
    public void limpiarAction(ActionEvent event) {
        clearFields();
    }

    //=======================================================================================================//
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            obtenerUsuario();
            showMiniAvatar();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //asignar valores a la columna ID en la tabla
        IdCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return new SimpleStringProperty(Integer.toString(param.getValue().getId()));
            }
        });

        //asignar valores a la columna NOMBRE en la tabla
        nombreCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return param.getValue().nombreProperty();
            }
        });

        //asignar valores a la columna APELLIDO 1 en la tabla
        apellido1Col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return param.getValue().apellido1Property();
            }
        });

        //asignar valores a la columna APELLIDO 2 en la tabla
        apellido2Col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return param.getValue().apellido2Property();
            }
        });

        //asignar valores a la columna PROVINCIA en la tabla
        provinciaCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return param.getValue().provinciaProperty();
            }
        });

        //asignar valores a la columna CANTON en la tabla
        cantonCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return param.getValue().cantonProperty();
            }
        });

        //asignar valores a la columna DISTRITO en la tabla
        distritoCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return param.getValue().distritoProperty();
            }
        });

        //asignar valores a la columna DIRECCION en la tabla
        direccionCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return param.getValue().direccionProperty();
            }
        });

        //asignar valores a la columna EMAIL en la tabla
        emailCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return param.getValue().emailProperty();
            }
        });

        //asignar valores a la columna TELEFONO en la tabla
        telCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFisico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteFisico, String> param) {
                return param.getValue().telefonoProperty();
            }
        });


        //Hacer un arraylist de tipo "observable" para que pueda mostrar datos en la tabla.
        //Estos clientes igualmente se estan registrando con el gestor en el void "agregarAccion"
        listClienteFi = FXCollections.observableArrayList();

        //Agregar los datos que ya estaban en el arraylist previamente
        try {
            InsertarDatosTabla();
        } catch (Exception e) {
            e.printStackTrace();
        }

//=======================================================================================================//


        //para editar, si se seleccionan datos de la tabla, pegarlos en el formulario
        clientFiTableView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {

            if (clientFiTableView.getSelectionModel().getSelectedItem() != null) {
                //el codigo no se puede editar,
                //esto es para que no hayan problemas de repeticion en la base de datos
                clientFiIdTF.setEditable(false);
                clientFiIdTF.setDisable(true);

                TreeItem tempClient = new TreeItem(newValue);
                showDetails(tempClient);
            }
        });


//=======================================================================================================//
        // Inicializar la columna nombre (el cliente se busca por nombre)
        nombreCol.setCellValueFactory(cellData -> cellData.getValue().nombreProperty());

        // Meter el arraylist en una lista filtrada
        FilteredList<ClienteFisico> filteredData = new FilteredList<>(listClienteFi, p -> true);

        //setear el filtro predictivo cada que cambie
        clientFiBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(clienteFisico -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Comparar los nombre de la tabla con el de la busqueda
                String lowerCaseFilter = newValue.toLowerCase();

                if (clienteFisico.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                }
                return false; // Does not match.
            });
        });

        //convertir la lista filtrada en lista ordenada
        SortedList<ClienteFisico> sortedData = new SortedList<>(filteredData);

        // hacer bind de la lista sorted en la tabla
        sortedData.comparatorProperty().bind(clientFiTableView.comparatorProperty());

        // Agregar la lista sorted a la tabla.
        clientFiTableView.setItems(sortedData);
    }
}