package controllers;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.ucenfotec.bl.dao.usuario.Usuario;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioController extends MainController implements Initializable {


    @FXML
    private JFXTextField userNombreTF;
    @FXML
    private JFXTextField userPrimerApellidoTF;
    @FXML
    private JFXTextField userSegundoApellidoTF;
    @FXML
    private JFXTextField userEmailTF;
    @FXML
    private JFXPasswordField userPassTF;
    @FXML
    private JFXTextField userIdTF;
    @FXML
    private JFXComboBox<String> userGeneroTF;

    @FXML
    private VBox avatarVbox;

    private ObservableList<Usuario> OlUser;


    ImageView imgViewAvatar ;
    String avatarImg = "";


    //muestra la imagen que sube el usuario
    public void addElementstoGUI() {

        imgViewAvatar = new ImageView();
        avatarVbox.getChildren().addAll(imgViewAvatar);
    }
    //=======================================================================================================//
    public void loadImg(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG =
                new FileChooser.ExtensionFilter("JPG files (*.JPG)", "*.JPG");
        FileChooser.ExtensionFilter extFilterjpg =
                new FileChooser.ExtensionFilter("jpg files (*.jpg)", "*.jpg");
        FileChooser.ExtensionFilter extFilterPNG =
                new FileChooser.ExtensionFilter("PNG files (*.PNG)", "*.PNG");
        FileChooser.ExtensionFilter extFilterpng =
                new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
        fileChooser.getExtensionFilters()
                .addAll(extFilterJPG, extFilterjpg, extFilterPNG, extFilterpng);

        //Show open file dialog
        File file = fileChooser.showOpenDialog(null);

        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imgViewAvatar.setImage(image);
            imgViewAvatar.setFitHeight(450);
            imgViewAvatar.setFitWidth(450);

            avatarImg = file.toString();


        } catch (IOException ex) {
            Logger.getLogger(SettingsController.class.getName()).log(Level.SEVERE, null, ex);
        }

    };

    //=======================================================================================================//

    //definir las opciones de tipo de proyecto
    ObservableList<String> genero = FXCollections.observableArrayList("Masculino", "Femenino");

    //metodo para verificar la informacion

    public void verificarAgregar(ActionEvent event) throws Exception {

        //verificar que la cedula sea un valor numerico.
        try{
            Integer.parseInt(userIdTF.getText());

            //como es monousuario, que no permita registrar mas de un usuario
            if (!gt.obtenerUsuarios().isEmpty()) {
                advertenciaMonoUsuario(event);

            }
            //verificar si la info ya esta registrada (q igual esto nunca va a pasar)
            else if (gt.buscarUsuario(userEmailTF.getText())) {

                infoRepetida(event);
            }
            //verificar si la info esta incompleta
            else if (userIdTF.getText().isEmpty() ||
                    userNombreTF.getText().isEmpty() ||
                    userPrimerApellidoTF.getText().isEmpty() ||
                    userSegundoApellidoTF.getText().isEmpty() ||
                    userEmailTF.getText().isEmpty() ||
                    userPassTF.getText().isEmpty() ||
                    userGeneroTF.getSelectionModel().isEmpty()) {

                infoIncompletaRegistro(event);

            } else {

                agregarAction();

                out.println(gt.obtenerUsuarios());
                usuarioRegistrado(event);
                //changeScreenButtonLogOut(event);
            }
        }
        //obtener error en consola, imprimir mensaje para usuario
        catch (NumberFormatException e){
            out.println(e.getMessage());
            numberExceptionModal(event);
        }

    }



    //=======================================================================================================//
    @FXML
    public void agregarAction() throws Exception {

        int id = Integer.parseInt(userIdTF.getText());

        //registrar user (despues se puede quitar el print, es solo para probar)
        gt.registrarUsuario(id, userNombreTF.getText(), userPrimerApellidoTF.getText(),
                    userSegundoApellidoTF.getText(), userEmailTF.getText(),
                    userGeneroTF.getSelectionModel().getSelectedItem(),
                    avatarImg, userPassTF.getText());

        OlUser.addAll(new Usuario(id, userNombreTF.getText(), userPrimerApellidoTF.getText(),
                userSegundoApellidoTF.getText(), userEmailTF.getText(),
                userGeneroTF.getSelectionModel().getSelectedItem(),
                avatarImg, userPassTF.getText()));

    }


    //metodo para insertar desde el inicio los datos en la tabla
    private void InsertarDatos() throws Exception {

        for (Usuario u : gt.obtenerUsuarios()) {

            //agregar lo que este en el arraylist a la tabla de tec
            OlUser.addAll(u);
        }
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //mostrar imagen subida
        addElementstoGUI();

        //Hacer un arraylist de tipo "observable" para que pueda mostrar datos.
        OlUser = FXCollections.observableArrayList();
        try {
            InsertarDatos();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //asignar opciones al combo genero
        userGeneroTF.setItems(genero);



    }

}
