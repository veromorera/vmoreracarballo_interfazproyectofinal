package controllers;

import cr.ac.ucenfotec.bl.dao.clientejuridico.ClienteJuridico;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;


public class ClienteJuController extends MainController implements Initializable {


    @FXML
    private JFXTextField clientJuBuscar;

    @FXML
    private JFXTextField clientJuNombreTF;

    @FXML
    private JFXTextField clientJuApellido1TF;

    @FXML
    private JFXTextField clientJuApellido2TF;

    @FXML
    private JFXTextField clientJuProvinciaTF;

    @FXML
    private JFXTextField clientJuCantonTF;

    @FXML
    private JFXTextField clientJuDistritoTF;

    @FXML
    private JFXTextField clientJuDireccionTF;

    @FXML
    private JFXTextField clientJuEmailTF;

    @FXML
    private JFXTextField clientJuTelefonoTF;

    @FXML
    private JFXTextField clientJuCedJuridicaTF;

    @FXML
    private JFXTextField clientJuIdTF;

    @FXML
    private JFXTextField clientJuNombreClienteTF;

    @FXML
    private TableView<ClienteJuridico> clientJuTableView;

    @FXML
    private TableColumn<ClienteJuridico, String> IdCol;

    @FXML
    private TableColumn<ClienteJuridico, String> ClienteCol;

    @FXML
    private TableColumn<ClienteJuridico, String> juridicaCol;

    @FXML
    private TableColumn<ClienteJuridico, String> nombreCol;

    @FXML
    private TableColumn<ClienteJuridico, String> apellido1Col;

    @FXML
    private TableColumn<ClienteJuridico, String> apellido2Col;

    @FXML
    private TableColumn<ClienteJuridico, String> provinciaCol;

    @FXML
    private TableColumn<ClienteJuridico, String> cantonCol;

    @FXML
    private TableColumn<ClienteJuridico, String> distritoCol;

    @FXML
    private TableColumn<ClienteJuridico, String> direccionCol;

    @FXML
    private TableColumn<ClienteJuridico, String> emailCol;

    @FXML
    private TableColumn<ClienteJuridico, String> telCol;

    private ObservableList<ClienteJuridico> listClienteJu;

    ImageView imgViewMiniAvatar;


    //=======================================================================================================//
//metodo para que se muestren los datos en la tabla (listar)
    private void showDetails(TreeItem<ClienteJuridico> treeItem) {

        //convertir cedula a numero para llenar formulario
        String idJuridica = Integer.toString(treeItem.getValue().getCedulaJuridica());
        String id = Integer.toString(treeItem.getValue().getId());

        clientJuCedJuridicaTF.setText(idJuridica);
        clientJuNombreClienteTF.setText(treeItem.getValue().getNombreCliente());
        clientJuIdTF.setText(id);
        clientJuNombreTF.setText(treeItem.getValue().getNombre());
        clientJuApellido1TF.setText(treeItem.getValue().getApellido1());
        clientJuApellido2TF.setText(treeItem.getValue().getApellido2());
        clientJuProvinciaTF.setText(treeItem.getValue().getProvincia());
        clientJuCantonTF.setText(treeItem.getValue().getCanton());
        clientJuDistritoTF.setText(treeItem.getValue().getDistrito());
        clientJuDireccionTF.setText(treeItem.getValue().getDireccion());
        clientJuEmailTF.setText(treeItem.getValue().getEmail());
        clientJuTelefonoTF.setText(treeItem.getValue().getTelefono());

    }

    //=======================================================================================================//
//metodo para verificar la informacion
    public void verificarAgregar(ActionEvent event) throws Exception {

        try{
            //verificar que la cedula sea un valor numerico.
            Integer.parseInt(clientJuIdTF.getText());
            Integer.parseInt(clientJuCedJuridicaTF.getText());


            //verificar si la info esta incompleta
            if (clientJuIdTF.getText().isEmpty() || clientJuNombreTF.getText().isEmpty() ||
                    clientJuApellido1TF.getText().isEmpty() || clientJuApellido2TF.getText().isEmpty() ||
                    clientJuProvinciaTF.getText().isEmpty() || clientJuCantonTF.getText().isEmpty() ||
                    clientJuDistritoTF.getText().isEmpty() || clientJuDireccionTF.getText().isEmpty() ||
                    clientJuEmailTF.getText().isEmpty() || clientJuTelefonoTF.getText().isEmpty() ||
                    clientJuCedJuridicaTF.getText().isEmpty() || clientJuNombreClienteTF.getText().isEmpty()){

                infoIncompletaRegistro(event);

            }//verificar si la info ya esta registrada
            else if (gt.buscarClienteJu(Integer.parseInt(clientJuCedJuridicaTF.getText()), clientJuNombreTF.getText())) {

                infoRepetida(event);
            } else {

                agregarAction();
            }
        }
        //obtener error en consola, imprimir mensaje para usuario
        catch (NumberFormatException e){
            out.println(e.getMessage());
            numberExceptionModal(event);
        }

    }

    //=======================================================================================================//
    //metodo para insertar desde el inicio los datos en la tabla
    private void InsertarDatosTabla() throws Exception {

        for (ClienteJuridico clientJu : gt.obtenerClientesJu()) {

            //agregar lo que este en el arraylist a la tabla de tec
            listClienteJu.addAll(clientJu);
        }
    }

//=======================================================================================================//
    @FXML
    private void agregarAction() throws Exception {

        int idJuridica = Integer.parseInt(clientJuCedJuridicaTF.getText());
        int id = Integer.parseInt(clientJuIdTF.getText());


        //registrar cliente
        gt.registrarClienteJu(idJuridica, clientJuNombreClienteTF.getText(),
                clientJuProvinciaTF.getText(), clientJuCantonTF.getText(),
                clientJuDistritoTF.getText(), clientJuDireccionTF.getText(),
                id, clientJuNombreTF.getText(), clientJuApellido1TF.getText(),
                clientJuApellido2TF.getText(), clientJuEmailTF.getText(),
                clientJuTelefonoTF.getText());

        //agregarla a la lista "observable"
        listClienteJu.addAll(new ClienteJuridico(idJuridica, clientJuNombreClienteTF.getText(),
                clientJuProvinciaTF.getText(), clientJuCantonTF.getText(),
                clientJuDistritoTF.getText(), clientJuDireccionTF.getText(),
                id, clientJuNombreTF.getText(), clientJuApellido1TF.getText(),
                clientJuApellido2TF.getText(), clientJuEmailTF.getText(),
                clientJuTelefonoTF.getText()));

        //limpiar formulario
        clearFields();
    }

    //=======================================================================================================//
    @FXML
    public void editarAction(ActionEvent event) throws Exception {

        //seleccionar la info de la tabla.
        TreeItem<ClienteJuridico> treeItem = new TreeItem<>(clientJuTableView.getSelectionModel().getSelectedItem());


        //validar si hay info seleccionada
        if (clientJuTableView.getSelectionModel().getSelectedItem()== null) {
            validarBotonEditar(event);

        } else {
            //convertirla en cliente
            ClienteJuridico cj = new ClienteJuridico(Integer.parseInt(clientJuCedJuridicaTF.getText()),
                    clientJuNombreClienteTF.getText(), clientJuProvinciaTF.getText(),
                    clientJuCantonTF.getText(), clientJuDistritoTF.getText(),
                    clientJuDireccionTF.getText(), Integer.parseInt(clientJuIdTF.getText()),
                    clientJuNombreTF.getText(), clientJuApellido1TF.getText(),
                    clientJuApellido2TF.getText(), clientJuEmailTF.getText(),
                    clientJuTelefonoTF.getText());

            //editarla con el gestor
            gt.editarClienteJu(cj.getCedulaJuridica(), cj.getNombreCliente(),
                    cj.getProvincia(), cj.getCanton(), cj.getDistrito(), cj.getDireccion(),
                    cj.getId(), cj.getNombre(), cj.getApellido1(), cj.getApellido2(),
                    cj.getEmail(), cj.getTelefono());



            //volverla a asignar en la tabla.
            treeItem.setValue(cj);

            //refrescar la pagina
            changeScreenButtonClienteJuridico(event);

        }
    }
    //=======================================================================================================//
    //metodo para limpiar los campos del formulario.
    private void clearFields() {

        clientJuCedJuridicaTF.setText("");
        clientJuNombreClienteTF.setText("");
        clientJuIdTF.setText("");
        clientJuNombreTF.setText("");
        clientJuApellido1TF.setText("");
        clientJuApellido2TF.setText("");
        clientJuProvinciaTF.setText("");
        clientJuCantonTF.setText("");
        clientJuDistritoTF.setText("");
        clientJuDireccionTF.setText("");
        clientJuEmailTF.setText("");
        clientJuTelefonoTF.setText("");
    }
    //=======================================================================================================//
    @FXML
    public void limpiarAction(ActionEvent event) {
        clearFields();
    }
//=======================================================================================================//

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            obtenerUsuario();
            showMiniAvatar();
        } catch (Exception e) {
            e.printStackTrace();
        }


        //asignar valores a la columna CEDULA JURIDICA en la tabla
        juridicaCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return new SimpleStringProperty(Integer.toString(param.getValue().getCedulaJuridica()));
            }
        });

        //asignar valores a la columna NOMBRE CLIENTE en la tabla
        ClienteCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().nombreClienteProperty();
            }
        });
        //asignar valores a la columna ID en la tabla
        IdCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return new SimpleStringProperty(Integer.toString(param.getValue().getId()));
            }
        });

        //asignar valores a la columna NOMBRE en la tabla
        nombreCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().nombreProperty();
            }
        });

        //asignar valores a la columna APELLIDO 1 en la tabla
        apellido1Col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().apellido1Property();
            }
        });

        //asignar valores a la columna APELLIDO 2 en la tabla
        apellido2Col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().apellido2Property();
            }
        });

        //asignar valores a la columna PROVINCIA en la tabla
        provinciaCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().provinciaProperty();
            }
        });

        //asignar valores a la columna CANTON en la tabla
        cantonCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().cantonProperty();
            }
        });

        //asignar valores a la columna DISTRITO en la tabla
        distritoCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().distritoProperty();
            }
        });

        //asignar valores a la columna DIRECCION en la tabla
        direccionCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().direccionProperty();
            }
        });

        //asignar valores a la columna EMAIL en la tabla
        emailCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().emailProperty();
            }
        });

        //asignar valores a la columna TELEFONO en la tabla
        telCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteJuridico, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ClienteJuridico, String> param) {
                return param.getValue().telefonoProperty();
            }
        });


        //Hacer un arraylist de tipo "observable" para que pueda mostrar datos en la tabla.
        //Estos clientes igualmente se estan registrando con el gestor en el void "agregarAccion"
        listClienteJu = FXCollections.observableArrayList();

        //Agregar los datos que ya estaban en el arraylist previamente
        try {
            InsertarDatosTabla();
        } catch (Exception e) {
            e.printStackTrace();
        }

//=======================================================================================================//


        //para editar, si se seleccionan datos de la tabla, pegarlos en el formulario
        clientJuTableView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {

            if (clientJuTableView.getSelectionModel().getSelectedItem() != null) {
                //el codigo no se puede editar,
                //esto es para que no hayan problemas de repeticion en la base de datos
                clientJuCedJuridicaTF.setEditable(false);
                clientJuCedJuridicaTF.setDisable(true);

                TreeItem tempClient = new TreeItem(newValue);
                showDetails(tempClient);
            }
        });


//=======================================================================================================//
        // Inicializar la columna nombre (el cliente se busca por nombre)
        nombreCol.setCellValueFactory(cellData -> cellData.getValue().nombreProperty());

        // Meter el arraylist en una lista filtrada
        FilteredList<ClienteJuridico> filteredData = new FilteredList<>(listClienteJu, p -> true);

        //setear el filtro predictivo cada que cambie
        clientJuBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(ClienteJuridico -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Comparar los nombre de la tabla con el de la busqueda
                String lowerCaseFilter = newValue.toLowerCase();

                if (ClienteJuridico.getNombreCliente().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                }
                return false; // Does not match.
            });
        });

        //convertir la lista filtrada en lista ordenada
        SortedList<ClienteJuridico> sortedData = new SortedList<>(filteredData);

        // hacer bind de la lista sorted en la tabla
        sortedData.comparatorProperty().bind(clientJuTableView.comparatorProperty());

        // Agregar la lista sorted a la tabla.
        clientJuTableView.setItems(sortedData);
    }
}
