package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.ucenfotec.bl.dao.usuario.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController extends MainController implements Initializable {

    //=======================================================================================================//
    //INICIAR SESION
    //=======================================================================================================//

    @FXML
    private JFXTextField user;
    @FXML
    private JFXPasswordField pass;
    @FXML
    private JFXButton login;
    @FXML
    private JFXButton signup;
    @FXML
    private JFXButton forgotPass;


    //metodo para iniciar sesion
    @FXML
    void makeLogin(ActionEvent event) throws Exception {
        String username = user.getText();
        String pasword = pass.getText();
        Usuario user = new Usuario();

        //si alguno de los campos está vacio
        if (username.isEmpty() || pasword.isEmpty()) {
            infoIncompletaLogin(event);
        }

        //si la info no coincide con el usuario registrado
        else if (!gt.buscarUsuario(username)) {
            infoIncorrectaLogin(event);
        }

        //si el email coincide con el usuario
        else if (gt.buscarUsuario(username)) {

            //obtener usuario
            for (Usuario tmpU : gt.findGetUsuario(username)) {
                user = tmpU;
            }
            //verificar que usuario y contrasenia coinciden
            if (user.getEmail().equals(username) && user.getContrasena().equals(pasword)) {
                System.out.println("Bienvenido");
                changeScreenButtonInfo(event);

            } else {
                infoIncorrectaLogin(event);
                System.out.println("Info incorrecta");

            }
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
